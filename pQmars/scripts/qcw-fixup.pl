#! /usr/bin/perl -w 

use strict; 

my $echo_prefix = "$ENV{HOME}/public_html/pQmars/qcw-prod/standalone";

my $command = 
    "echo \"SetEnv COREWORLD_BIN $echo_prefix/bin\" ".
    ">$echo_prefix/admin/.htaccess"; 

print "$command\n";
system ($command); 

$command =~ s/qcw-prod/qcw-dev/g; 
print "$command\n";
system ($command); 

$command =~ s/admin/qcw/g; 
print "$command\n";
system ($command); 

$command =~ s/qcw-dev/qcw-prod/g; 
print "$command\n";
system ($command); 
