#!/usr/bin/perl -w 

use strict; 

use Getopt::Std;

my %args; 
getopts("fgpdsc", \%args);

my @commands; 

sub sync {
    
    my $i = shift(@_);
    my $j = shift(@_);
    my $k = shift(@_); 
    my $command; 

    if ($args{g}) { 
	$command = 
	    "rsync -rlpvcn $k $i:$j/ $j/";
    }
    else {
	$command = 
	    "rsync -rlpvcn $k $j/ $i:$j/";
    }
    $command =~ s/$j/qcw-prod\/$j/g;
    push (@commands, $command);     

    $command =~ s/qcw-prod/qcw-dev/g;
    push (@commands, $command);  
}


my $qcw = "$ENV{HOME}/public_html/pQmars/"; 

print "***$qcw***\n"; 
chdir $qcw; 


if ($args{s}) {
    push (@commands, "rsync -rlpvcn --delete --exclude CVS".
	  "qcw-prod/standalone/src/ qcw-prod/cli/src/"); 
    push (@commands, "rsync -rlpvcn --delete --exclude CVS".
	  "qcw-prod/standalone/src/ qcw-prod/cli-srv/src/"); 
}
if ($args{p}) {
    push (@commands, "rsync -rlpvcn ".
	  "--delete --exclude CVS --exclude .htaccess ".
	  "qcw-prod/ qcw-dev/"); 
}
if ($args{d}) {
    push (@commands, "rsync -rlpvcn ".
	  "--delete --exclude CVS --exclude .htaccess ".
	  "qcw-dev/ qcw-prod/"); 
}

sync ('wsa2@orchestra.med.harvard.edu', 'cli-srv', 
      '--delete --exclude CVS --exclude .htaccess');

sync ('pqmars@karuna', 'standalone', 
      '--delete --exclude CVS --exclude .htaccess'); 

foreach my $command (@commands) {  
    if ($args{f}) {
	$command =~ s/-rlpvcn/-rlpvc/;
    }
    print "\n***$command***\n"; 
    system ($command); 
}

if ($args{c}) {
    system ('cvs -z3 -d:ext:alexanderwait@'.
	    'cvs.sourceforge.net:/cvsroot/corewar ci');
}
