#!/usr/bin/perl -w

$intervention = 
    "~/bin/intervention.txt"; 
$pQmarsXwin = 
    "~/bin/pQmars-xwin";

$QCWout = 
    "~/bin/out.qcw"; 

$World = 
    "00000001.qcw.gz.bz2"; 

$command = 
    "tar -Ojxf $ARGV[0] $World | bzcat | zcat | ".
    "cat - $intervention | $pQmarsXwin".
    ' -d -@ - -@ - '.
    ">$QCWout";

print "$command\n";

system ($command); 


 
