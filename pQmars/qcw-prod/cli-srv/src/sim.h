/* pMARS -- a portable Memory Array Redcode Simulator
 * Copyright (C) 1993-1996 Albert Ma, Na'ndor Sieben, Stefan Strack and Mintardjo Wangsawidjaja
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * sim.h: header for sim.c, cdb.c and display files
 */

/* ASW -- This comment is mostly out of date... 
 * 
 * IR=memory[progCnt]
 * for the currently executing warrior
 * progCnt is the current cell being executed (operand decoding has not
 * occured yet)
 * W->taskHead is the next instruction in the queue (after the current inst)
 * W->tasks is the number of processes running
 * for the other warrior:  W->nextWarrior
 * W->nextWarrior->taskHead is the next instruction in the queue
 * W->nextWarrior->tasks is the number of processes running
 */

#ifndef SIM_INCLUDED
#define SIM_INCLUDED
#endif

#ifdef XWINGRAPHX
extern int xWinTextLines;
extern int printAttr;
#define         WAIT 1
#define         NOWAIT 0
#endif

#ifdef SDLGRAPHX
extern int printAttr;
#define         WAIT 1
#define         NOWAIT 0
#endif

#if defined(XWINGRAPHX) || defined(SDLGRAPHX) 
#define         PC1_ATTR 1
#define         PC2_ATTR 2
#endif //tells grputs that print_core() is printing PC1 or 2 
       //ASW -- figure out what this means... 


//ASW -- globals used mostly in sim.c, cdb.c ... very messy! ...

//extern int epoch;
extern ADDR_T progCnt;    /* program counter */

extern warrior_struct *W;        /* running warrior */

extern ADDR_T *taskQueue;
extern ADDR_T *endQueue;
extern U32_T totaltask;

extern mem_struct  *memory;

extern int curCore;

