/* pQmars -- a Portable Quantum Memory Array Redcode Simulator
 *
 * Copyright (C) 2003-2004 Alexander (Sasha) Wait and others
 *                          --please see AUTHORS for details 
 *                         
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307,  USA.  
 *
 */

#include "global.h"   // necessary for *GRAPHX 
#include "sim.h"      // OK? (what is really needed?) 
#include "rand.h"     // link with ISAAC


#ifdef XWINGRAPHX
#include "xwindisp.c" // ugly! (display_* is all we need...) 
#else
#ifdef SDLGRAPHX
#include "sdldisp.c"  // ditto 
#else                                /* !GRAPHX */
#define display_init()
#define display_clear()
#define display_read(addr)
#define display_write(addr)
#define display_dec(addr)
#define display_inc(addr)
#define display_exec(addr)
#define display_spl(warrior,tasks)
#define display_dat(address,warrior,tasks)
#define display_die(warnum)
#define display_close()
#define display_cycle()
#define display_push(val)
#endif                             
#endif 



//ASW -- globals for '94 style execution -- very broken ... 
extern warrior_struct *multi_warrior[MAXN];  //declared in sim.c  
extern ADDR_T *multi_taskQueue[MAXN*MAXWARRIOR]; //declared in sim.c 
extern mem_struct *multi_memory[]; 

//ASW -- dependency on "memory" to be removed... 


#define OP(opcode,modifier) (opcode<<8)+modifier
#define ADDMOD(A,B,C) do { \
  if ((C=(int) A+B)>=qcw->coreSize) C-=qcw->coreSize; } while (0)

#define SUBMOD(A,B,C) do { \
  if ((C=(int) A-B)<0) C+=qcw->coreSize; } while (0)

/*
 * exec_cycle -- do one step (in one cycle)  
 *        
 * expects (and returns) a pointer into qcw->molecules 
 *
 */
int exec_cycle (mem_struct *IR_ptr) {

  static int addrA, addrB, temp, temp2, temp3, xPos, yPos, zPos, cPos;
  
  static ADDR_T *offsPtr, AA_Value, AB_Value;

  static mem_struct IR, *tempPtr, *destPtr, *addrA_ptr, *addrB_ptr;  
  

  /*
   * evaluate A operand.  This is the hardest part of the entire
   * simulator code. ASW -- also see cdb.c 
   *
   * Obfuscated C code at it's finest!  Here be dragons... 
   *   
   */

  IR = *IR_ptr; 
  
  AB_Value = IR.A_value; // necessary if B-mode is immediate

  if (IR.A_mode != (FIELD_T) IMMEDIATE) {
    ADDMOD(IR.A_value, progCnt, addrA);
    tempPtr = &memory[addrA];
    
    if (IR.A_mode != (FIELD_T) DIRECT) {
      if (INDIR_A(IR.A_mode)) {
	IR.A_mode = RAW_MODE(IR.A_mode);
	offsPtr = &(tempPtr->A_value);
      } 
      else
	offsPtr = &(tempPtr->B_value);
	  
      temp = *offsPtr;

      if (IR.A_mode == (FIELD_T) PREDECR) {
	if (--temp < 0)
	  temp = POWER_OF_TWO(qcw->C_C)-1;
	*offsPtr = temp;
	display_dec(addrA);
      }
	
      display_read(addrA);

#ifndef SERVER
      temp2 = addrA;        /* make the trace accurate */
#endif
      
      ADDMOD(temp, addrA, addrA);
      
      destPtr = &memory[addrA];       //new

      AA_Value = destPtr->A_value;

      IR.A_value = destPtr->B_value;
		
      if (IR.A_mode == (FIELD_T) POSTINC) {
	if (++temp == qcw->coreSize)
	  temp = 0;
	*offsPtr = temp;

#ifndef SERVER
	display_inc(temp2);
#endif
      }
    } 
    else {
      IR.A_value = tempPtr->B_value;
      AA_Value = tempPtr->A_value;
    }
  } 
  else {
    AA_Value = IR.A_value;
    addrA = progCnt;
    IR.A_value = IR.B_value;
  }
  
  /*
   * evaluate B operand.  
   *
   */

  if (IR.B_mode != (FIELD_T) IMMEDIATE) {
    ADDMOD(IR.B_value, progCnt, addrB);
    tempPtr = &memory[addrB];
    
    if (IR.B_mode != (FIELD_T) DIRECT) {
      if (INDIR_A(IR.B_mode)) {
	IR.B_mode = RAW_MODE(IR.B_mode);
	offsPtr = &(tempPtr->A_value);
      } 
      else
	offsPtr = &(tempPtr->B_value);
      
      temp = *offsPtr;

      if (IR.B_mode == (FIELD_T) PREDECR) {
	if (--temp < 0)
	  temp = POWER_OF_TWO(qcw->C_C)-1;
	*offsPtr = temp;
	display_dec(addrB);
      }
	   
      display_read(addrB);

#ifndef SERVER 
      temp2 = addrB;
#endif
      
      ADDMOD(temp, addrB, addrB);
		
      destPtr = &memory[addrB];

      AB_Value = destPtr->A_value;

		
      IR.B_value = destPtr->B_value;
		
      if (IR.B_mode == (FIELD_T) POSTINC) {
	if (++temp == qcw->coreSize)
	  temp = 0;
	*offsPtr = temp;

#ifndef SERVER
	display_inc(temp2);
#endif
      }
    } 
    else {
      AB_Value = tempPtr->A_value;
		    
      IR.B_value = tempPtr->B_value;
    }
  } 
  else
    addrB = progCnt;

#define ADDRA_AVALUE AA_Value
#define ADDRB_AVALUE AB_Value
	
  /*
   * addrA holds the A-pointer; addrB holds the B-pointer
   * 
   * ADDRA_value holds the A operand of the A-pointer cell 
   * ADDRB_value holds the A operand of the B-pointer cell.
   *
   * IR.A_value holds the B operand of the A-pointer cell.  
   * IR.B_value holds the B operand of the B-pointer cell.
   *
   */
	  


  
  /* ASW -- pre-processing for "*D" modifiers */
  
  if (IR.opcode & 1) {

    IR.opcode &= 0xFFFe;
    
    molecule_to_cartesian(memory, &xPos, &yPos, &zPos, &cPos); 

    cPos = addrB; 

    if (cPos < POWER_OF_TWO(qcw->C_C-2)) {
      //north 
      if (++yPos == POWER_OF_TWO(qcw->C_Y)) {  
	//remove_organism (memory+progCnt);
	return(-1); 
      }
    }
    else if ( cPos < POWER_OF_TWO (qcw->C_C-1)) {
      //east
      if (++xPos == POWER_OF_TWO(qcw->C_X)) {  
	//remove_organism (memory+progCnt);
	return(-1); 
      }
      
    }
    else if (cPos < POWER_OF_TWO (qcw->C_C)-POWER_OF_TWO(qcw->C_C-2)) { 
      //south
      if (--yPos == -1) {  
	//remove_organism (memory+progCnt);
	return(-1); 
      }
    }
    else {
      //west
      if (--xPos == -1) {  
	//remove_organism (memory+progCnt);
	return(-1); 
      }
    }
    addrA_ptr = memory+addrA; 

    ADDMOD (cPos, POWER_OF_TWO(qcw->C_C-1), addrB);     
    addrB_ptr = cartesian_to_molecule (xPos, yPos, zPos, addrB); 
  } 
  else {
    addrA_ptr = memory+addrA; 
    addrB_ptr = memory+addrB; 
  }

  /* pre-processing for "*E" modifiers */
 
  if (IR.opcode & 2) {    
    IR.opcode &= 0xFFFD;
    --IR_ptr->P_value;
    --qcw->bP; 
    ++qcw->cP;    
  } 
    
  /* execute it! */

  display_exec(progCnt);
  switch (IR.opcode) {
    
  case OP(MOV, mA):
    display_read(addrA);
    addrB_ptr->A_value = ADDRA_AVALUE;
    //display_write(addrB);
    break;
    
  case OP(MOV, mF):  
    addrB_ptr->A_value = ADDRA_AVALUE;
  
    /* FALLTHRU */
  
  case OP(MOV, mB):
    display_read(addrA);
    addrB_ptr->B_value = IR.A_value;
    //display_write(addrB); 
    break;
    
  case OP(MOV, mAB):
    display_read(addrA);
    addrB_ptr->B_value = ADDRA_AVALUE;
    //display_write(addrB);
    break;
    
  case OP(MOV, mX):
    addrB_ptr->B_value = ADDRA_AVALUE;
   
    /* FALLTHRU */
  
  case OP(MOV, mBA):
    display_read(addrA);
    addrB_ptr->A_value = IR.A_value;
    //display_write(addrB);
    break;


    
  case OP(ADD, mA):
    display_read(addrA);
    ADDMOD(ADDRB_AVALUE, ADDRA_AVALUE, temp);
    addrB_ptr->A_value = temp;
    //display_write(addrB);
    break;
    
  case OP(ADD, mI):
  case OP(ADD, mP):
  case OP(ADD, mQ):
  case OP(ADD, mF):
    
    ADDMOD(ADDRB_AVALUE, ADDRA_AVALUE, temp);
    addrB_ptr->A_value = temp;
    
    /* FALLTHRU */
  
  case OP(ADD, mB):
    display_read(addrA);
    ADDMOD(IR.B_value, IR.A_value, temp);
    addrB_ptr->B_value = temp;
    //display_write(addrB);
    break;
    
  case OP(ADD, mAB):
    display_read(addrA);
    ADDMOD(IR.B_value, ADDRA_AVALUE, temp);
    addrB_ptr->B_value = temp;
    //display_write(addrB);
    break;
    
  case OP(ADD, mX):
    ADDMOD(IR.B_value, ADDRA_AVALUE, temp);
    addrB_ptr->B_value = temp;
    /* FALLTHRU */
  case OP(ADD, mBA):
    display_read(addrA);
    ADDMOD(ADDRB_AVALUE, IR.A_value, temp);
    addrB_ptr->A_value = temp;
    //display_write(addrB);
    break;

    
    
  case OP(SUB, mA):
    display_read(addrA);
    SUBMOD(ADDRB_AVALUE, ADDRA_AVALUE, temp);
    addrB_ptr->A_value = temp;
    //display_write(addrB);
    break;
    
  case OP(SUB, mI):
  case OP(SUB, mP):
  case OP(SUB, mQ):
  case OP(SUB, mF):
    SUBMOD(ADDRB_AVALUE, ADDRA_AVALUE, temp);
    addrB_ptr->A_value = temp;

    /* FALLTHRU */

  case OP(SUB, mB):
    display_read(addrA);
    SUBMOD(IR.B_value, IR.A_value, temp);
    addrB_ptr->B_value = temp;
    //display_write(addrB);
    break;
	  
  case OP(SUB, mAB):
    display_read(addrA);
    SUBMOD(IR.B_value, ADDRA_AVALUE, temp);
    addrB_ptr->B_value = temp;
    //display_write(addrB);
    break;
    
  case OP(SUB, mX):
    SUBMOD(IR.B_value, ADDRA_AVALUE, temp);
    addrB_ptr->B_value = temp;
    
    /* FALLTHRU */
  
  case OP(SUB, mBA):
    display_read(addrA);
    SUBMOD(ADDRB_AVALUE, IR.A_value, temp);
    addrB_ptr->A_value = temp;
    //display_write(addrB);
    break;
    

    //cast prevents overflow 
  
  case OP(MUL, mA):
    display_read(addrA);
    addrB_ptr->A_value =
      (U32_T) ADDRB_AVALUE *ADDRA_AVALUE % qcw->coreSize;
    //display_write(addrB);
    break;
    
  case OP(MUL, mI):
  case OP(MUL, mP):
  case OP(MUL, mQ):
  case OP(MUL, mF):
    addrB_ptr->A_value =
      (U32_T) ADDRB_AVALUE *ADDRA_AVALUE % qcw->coreSize;
    
    /* FALLTHRU */
  
  case OP(MUL, mB):
    display_read(addrA);
    addrB_ptr->B_value =
      (U32_T) IR.B_value * IR.A_value % qcw->coreSize;
    //display_write(addrB);
    break;
    
  case OP(MUL, mAB):
    display_read(addrA);
    addrB_ptr->B_value =
      (U32_T) IR.B_value * ADDRA_AVALUE % qcw->coreSize;
    //display_write(addrB);
    break;
    
  case OP(MUL, mX):
    addrB_ptr->B_value =
      (U32_T) IR.B_value * ADDRA_AVALUE % qcw->coreSize;
    
    /* FALLTHRU */
  
  case OP(MUL, mBA):
    display_read(addrA);
    addrB_ptr->A_value =
      (U32_T) ADDRB_AVALUE *IR.A_value % qcw->coreSize;
    //display_write(addrB);
    break;
    

    
  case OP(DIV, mA):
    display_read(addrA);
    if (!ADDRA_AVALUE)
      goto die;
    addrB_ptr->A_value = ADDRB_AVALUE / ADDRA_AVALUE;
    //display_write(addrB);
    break;
	
  case OP(DIV, mB):
    display_read(addrA);
    if (!IR.A_value)
      goto die;
    addrB_ptr->B_value = IR.B_value / IR.A_value;
    //display_write(addrB);
    break;
    
  case OP(DIV, mAB):
    display_read(addrA);
    if (!ADDRA_AVALUE)
      goto die;
    
    addrB_ptr->B_value = IR.B_value / ADDRA_AVALUE;
    //display_write(addrB);
    break;
    
  case OP(DIV, mBA):
    display_read(addrA);
    if (!IR.A_value)
      goto die;
    addrB_ptr->A_value = ADDRB_AVALUE / IR.A_value;
    //display_write(addrB);
    break;
    
  case OP(DIV, mI):
  case OP(DIV, mP):
  case OP(DIV, mQ):
  case OP(DIV, mF):
    display_read(addrA);
    if (ADDRA_AVALUE) {
      addrB_ptr->A_value = ADDRB_AVALUE / ADDRA_AVALUE;
      //display_write(addrB);
      if (!IR.A_value)
	goto die;
      addrB_ptr->B_value = IR.B_value / IR.A_value;
      //display_write(addrB);
      break;
    } 
    else {
      if (!IR.A_value)
	goto die;
      addrB_ptr->B_value = IR.B_value / IR.A_value;
      //display_write(addrB);
      goto die;
    }


  case OP(DIV, mX):
    display_read(addrA);
    if (IR.A_value) {
      addrB_ptr->A_value = ADDRB_AVALUE / IR.A_value;
      //display_write(addrB);
      if (!ADDRA_AVALUE)
	goto die;
      addrB_ptr->B_value = IR.B_value / ADDRA_AVALUE;
      //display_write(addrB);
      break;
    } 
    else {
      if (!ADDRA_AVALUE)
	goto die;
      addrB_ptr->B_value = IR.B_value / ADDRA_AVALUE;
      //display_write(addrB);
      goto die;
    }


	  
  case OP(MOD, mA):
    display_read(addrA);
    if (!ADDRA_AVALUE)
      goto die;
    addrB_ptr->A_value = ADDRB_AVALUE % ADDRA_AVALUE;
    //display_write(addrB);
    break;
    
  case OP(MOD, mB):
    display_read(addrA);
    if (!IR.A_value)
      goto die;
    addrB_ptr->B_value = IR.B_value % IR.A_value;
    //display_write(addrB);
    break;
    
  case OP(MOD, mAB):
    display_read(addrA);
    if (!ADDRA_AVALUE)
      goto die;
    addrB_ptr->B_value = IR.B_value % ADDRA_AVALUE;
    //display_write(addrB);
    break;
    
  case OP(MOD, mBA):
    display_read(addrA);
    if (!IR.A_value)
      goto die;
    addrB_ptr->A_value = ADDRB_AVALUE % IR.A_value;
    //display_write(addrB);
    break;
    
  case OP(MOD, mI):
  case OP(MOD, mP):
  case OP(MOD, mQ):
  case OP(MOD, mF):
    display_read(addrA);
    if (ADDRA_AVALUE) {
      addrB_ptr->A_value = ADDRB_AVALUE % ADDRA_AVALUE;
      //display_write(addrB);
      if (!IR.A_value)
	goto die;
      addrB_ptr->B_value = IR.B_value % IR.A_value;
      //display_write(addrB);
      break;
    } 
    else {
      if (!IR.A_value)
	goto die;
      addrB_ptr->B_value = IR.B_value % IR.A_value;
      //display_write(addrB);
      goto die;
    }
	  
  case OP(MOD, mX):
    display_read(addrA);
    if (IR.A_value) {
      addrB_ptr->A_value = ADDRB_AVALUE % IR.A_value;
      //display_write(addrB);
      if (!ADDRA_AVALUE)
	goto die;
      addrB_ptr->B_value = IR.B_value % ADDRA_AVALUE;
      //display_write(addrB);
      break;
    } 
    else {
      if (!ADDRA_AVALUE)
	goto die;
      addrB_ptr->B_value = IR.B_value % ADDRA_AVALUE;
      //display_write(addrB);
      goto die;
    }
	  
  

  case OP(JMZ, mA):
  case OP(JMZ, mBA):
    display_read(addrB);
    if (ADDRB_AVALUE)
      break;
    return(addrA);
    
  case OP(JMZ, mI):
  case OP(JMZ, mP):
  case OP(JMZ, mQ):
  case OP(JMZ, mX):
  case OP(JMZ, mF):
    
    display_read(addrB);
    if (ADDRB_AVALUE)
      break;
    /* FALLTHRU */
  case OP(JMZ, mB):
  case OP(JMZ, mAB):
    display_read(addrB);
    if (IR.B_value)
      break;
    return(addrA);
    
	
  
  case OP(JMN, mA):
  case OP(JMN, mBA):
    display_read(addrB);
    if (!ADDRB_AVALUE)
      break;
    return(addrA);
    
  case OP(JMN, mB):
  case OP(JMN, mAB):
    display_read(addrB);
    if (!IR.B_value)
      break;
    return(addrA);
        
  case OP(JMN, mI):
  case OP(JMN, mP):
  case OP(JMN, mQ):
  case OP(JMN, mX):
  case OP(JMN, mF):
    display_read(addrB);
    if (!ADDRB_AVALUE && !IR.B_value)
      break;
    return(addrA);
   	


  case OP(DJN, mA):
  case OP(DJN, mBA):
    if (ISNEG(--addrB_ptr->A_value))
      addrB_ptr->A_value = POWER_OF_TWO(qcw->C_C)-1;
    display_dec(addrB);
    if (AB_Value == 1)
      break;
    return(addrA);

  case OP(DJN, mB):
  case OP(DJN, mAB):
    if (ISNEG(--addrB_ptr->B_value))
      addrB_ptr->B_value = POWER_OF_TWO(qcw->C_C)-1;
    display_dec(addrB);
    if (IR.B_value == 1)
      break;
    return(addrA);
        
  case OP(DJN, mI):
  case OP(DJN, mF):
  case OP(DJN, mX):
    if (ISNEG(--addrB_ptr->B_value))
      addrB_ptr->B_value = POWER_OF_TWO(qcw->C_C)-1;
    if (ISNEG(--addrB_ptr->A_value))
      addrB_ptr->A_value = POWER_OF_TWO(qcw->C_C)-1;
    display_dec(addrB);
    if ((AB_Value == 1) && (IR.B_value == 1))
      break;
    return(addrA);
    

   
  case OP(IJN, mA):
  case OP(IJN, mBA):    
    //ASW -- TO DO 
    break; 
    
  case OP(IJN, mB):
  case OP(IJN, mAB):    
    //ASW -- TO DO
    break;
    
  case OP(IJN, mI):
  case OP(IJN, mF):
  case OP(IJN, mX):
    // ASW -- TO DO 
    break; 

  case OP(DJN, mP): 
       
    if (addrB_ptr->P_value) {
      
      --qcw->bP; 
      --addrB_ptr->P_value;       

      ++IR_ptr->P_value; 
      ++qcw->bP; 
      
      return(addrA); 

    }
    else {
      if (addrB_ptr->tag) {
	set_empty_molecule(addrB_ptr); 
	--qcw->tP; 
	
	++IR_ptr->P_value; 
	++qcw->bP; 

	return(addrA);   
      }
      break; 
    }
 
  
  case OP(IJN, mP):
 
    if (addrB_ptr->P_value ) { 
         
      if (addrB_ptr->tag) {
	*(qcw->priv_last_new_slice++) = addrB_ptr;
	++addrB_ptr->P_value; 
	++qcw->bP; 
      }
      else {
	++addrB_ptr->P_value; 
	++qcw->bP;
      }
      --IR_ptr->P_value; // negative is still OK
      --qcw->bP; 
      
      return(addrA); 
    }
    else {
      
      if (addrB_ptr->tag) {
	*(qcw->priv_last_new_slice++) = addrB_ptr;
	++addrB_ptr->P_value; 
	++qcw->bP; 
      }
      else {
	++addrB_ptr->P_value; 
	++qcw->bP;
      }
      --IR_ptr->P_value; // negative is still OK
      --qcw->bP; 
      
      break;
    }

  case OP(IJN, mQ):	  
    temp = 1; 
    goto djnMq;
    
  case OP(DJN, mQ):
    temp = 0; 
    
  djnMq: 

    if (addrB_ptr->Q_value) {	      
      addrB_ptr->Q_value = temp; 
      return(addrA);
    }
    addrB_ptr->Q_value = temp; 
    
    break;


  case OP(SEQ, mA):
  case OP(CMP, mA):
    display_read(addrB);
    display_read(addrA);
    if (ADDRB_AVALUE != ADDRA_AVALUE)
      break;
    goto skip; 
    
  case OP(SEQ, mQ): 
    display_read(addrB);
    display_read(addrA);

    if ( addrB_ptr->Q_value != memory[addrA].Q_value )
      break;
    goto skip; 

  case OP(SEQ, mI):
  case OP(CMP, mI):
    display_read(addrB);
    display_read(addrA);
    
    if ((addrB_ptr->opcode != memory[addrA].opcode) ||
	(addrB_ptr->A_mode != memory[addrA].A_mode) ||
	(addrB_ptr->B_mode != memory[addrA].B_mode))
      break;
    
    /* FALLTHRU */
    
  case OP(SEQ, mF):
  case OP(SEQ, mP):
  case OP(CMP, mF):
  case OP(CMP, mP):
    display_read(addrB);
    display_read(addrA);
    if (ADDRB_AVALUE != ADDRA_AVALUE)
      break;

    /* FALLTHRU */

  case OP(SEQ, mB):
  case OP(CMP, mB):
    display_read(addrB);
    display_read(addrA);
    if (IR.B_value != IR.A_value)
      break;
    goto skip;
    
  case OP(SEQ, mAB):
  case OP(CMP, mAB):
    display_read(addrB);
    display_read(addrA);
    if (IR.B_value != ADDRA_AVALUE)
      break;
    goto skip;
    
  case OP(SEQ, mX):
  case OP(CMP, mX):
    display_read(addrB);
    display_read(addrA);
    if (IR.B_value != ADDRA_AVALUE)
      break;
    
    /* FALLTHRU */
  
  case OP(SEQ, mBA):
  case OP(CMP, mBA):
    display_read(addrB);
    display_read(addrA);
    if (ADDRB_AVALUE != IR.A_value)
      break;
    goto skip;



  case OP(SNE, mA):
    display_read(addrB);
    display_read(addrA);
    if (ADDRB_AVALUE != ADDRA_AVALUE)
      goto skip;
    break;
	
  case OP(SNE, mI):
    display_read(addrB);
    display_read(addrA);
    
    if ((addrB_ptr->opcode != memory[addrA].opcode) ||
	(addrB_ptr->A_mode != memory[addrA].A_mode) ||
	(addrB_ptr->B_mode != memory[addrA].B_mode) ||
	(addrB_ptr->Q_value != memory[addrA].Q_value))
      goto skip;
    
    /* FALLTHRU */

  case OP(SNE, mP):
  case OP(SNE, mQ):
  case OP(SNE, mF):
    display_read(addrB);
    display_read(addrA);
    if (ADDRB_AVALUE != ADDRA_AVALUE)
      goto skip;

    /* FALLTHRU */

  case OP(SNE, mB):
    display_read(addrB);
    display_read(addrA);
    if (IR.B_value != IR.A_value)
      goto skip;
    break;

  case OP(SNE, mAB):
    display_read(addrB);
    display_read(addrA);
    if (IR.B_value != ADDRA_AVALUE)
      goto skip;
    break;

  case OP(SNE, mX):
    display_read(addrB);
    display_read(addrA);
    if (IR.B_value != ADDRA_AVALUE)
      goto skip;
    
    /* FALLTHRU */
  
  case OP(SNE, mBA):
    display_read(addrB);
    display_read(addrA);
    if (ADDRB_AVALUE != IR.A_value)
      goto skip;
    break;

  
  
  case OP(SLT, mA):
    display_read(addrB);
    display_read(addrA);
    if (ADDRB_AVALUE <= ADDRA_AVALUE)
      break;
    goto skip;
    
  case OP(SLT, mF):
  case OP(SLT, mI):
    display_read(addrB);
    display_read(addrA);
    if (ADDRB_AVALUE <= ADDRA_AVALUE)
      break;

    /* FALLTHRU */

  case OP(SLT, mB):
    display_read(addrB);
    display_read(addrA);
    if (IR.B_value <= IR.A_value)
      break;
    goto skip; 
    
  case OP(SLT, mAB):
    display_read(addrB);
    display_read(addrA);
    if (IR.B_value <= ADDRA_AVALUE)
      break;
    goto skip;
    
  case OP(SLT, mX):
    display_read(addrB);
    display_read(addrA);
    if (IR.B_value <= ADDRA_AVALUE)
      break;
    
    /* FALLTHRU */
	
  case OP(SLT, mBA):
    display_read(addrB);
    display_read(addrA);
    if (ADDRB_AVALUE <= IR.A_value)
      break;
    goto skip;
    
    
  case OP(JMP, mA):
  case OP(JMP, mB):
  case OP(JMP, mAB):
  case OP(JMP, mBA):
  case OP(JMP, mI):
  case OP(JMP, mP):
  case OP(JMP, mQ):
  case OP(JMP, mX):
  case OP(JMP, mF):
    
      return(addrA);

            
   
  case OP(SPL, mA):
  case OP(SPL, mB):
  case OP(SPL, mAB):
  case OP(SPL, mBA):
  case OP(SPL, mI):
  case OP(SPL, mP):
  case OP(SPL, mQ):
  case OP(SPL, mX):
  case OP(SPL, mF):
    
    // NOP for 2004 exec 
    if (IR.tag) 
      break;
    
    return (-1);


  case OP(DAT, mA):
  case OP(DAT, mB):
  case OP(DAT, mAB):
  case OP(DAT, mBA):
  case OP(DAT, mI):
  case OP(DAT, mP):
  case OP(DAT, mQ):
  case OP(DAT, mX):
  case OP(DAT, mF):
    
  die:
    // '04 style exec... 
    if (IR_ptr->tag) {
	return (progCnt);
    }
    else {
      //display_dat(progCnt, curWarrior, W->tasks);
      // '94 style exec...  
      --W->tasks; 
      //if (!W->tasks) 
      //  score94[curFilm][curWarrior]--;    
      return (-1); 
    }
	    
  case OP(NOP, mA):
  case OP(NOP, mB):
  case OP(NOP, mAB):
  case OP(NOP, mBA):
  case OP(NOP, mI):
  case OP(NOP, mP):
  case OP(NOP, mQ):
  case OP(NOP, mX):
  case OP(NOP, mF):
    break;
    


  case OP(QOP, mA):
  case OP(QOP, mBA): 
    temp = ADDRB_AVALUE;
    temp2 = 0; 
    goto quantum_op; 
    
  case OP(QOP, mB): 
  case OP(QOP, mAB): 
    temp= IR.B_value; 
    temp2 = 0;
    goto quantum_op; 
    
  case OP(QOP, mX):
    temp = IR.B_value;
    temp2 = ADDRB_AVALUE; 
    goto quantum_op;   
    
  case OP(QOP, mI):
  case OP(QOP, mP):
  case OP(QOP, mQ):
  case OP(QOP, mF):
    
    temp = ADDRB_AVALUE; 
    temp2 = IR.B_value; 
    
  quantum_op: 
    
    display_read(addrA); 
   
    
    break;
    
  case OP(QCN, mA):
  case OP(QCN, mB):
  case OP(QCN, mAB):
  case OP(QCN, mBA):
  case OP(QCN, mI):
  case OP(QCN, mP):
  case OP(QCN, mQ):
  case OP(QCN, mX):
  case OP(QCN, mF):
    
    //display_read(addrA);
    //display_read(addrB);
    
 
    break;
     
    
  case OP(MOV, mP):
    if (!addrB_ptr->P_value && memory[addrA].P_value) 
	*(qcw->priv_last_new_slice++) = addrB_ptr;

    temp = IR.P_value-memory[addrA].P_value+addrB_ptr->P_value; 
    
    if (temp > 0) {          
      addrB_ptr->P_value = memory[addrA].P_value; 
      IR_ptr->P_value = temp; 
    }
    else {
      addrB_ptr->P_value = memory[addrA].P_value+temp;
      IR_ptr->P_value = 0;      
      return (-1); 
    }     
    break; 
    
    
  
  case OP(MOV, mQ):
    
    display_read(addrA);
    
  
    addrB_ptr->Q_value = memory[addrA].Q_value;
    
    //display_write(addrB);


    break;

  case OP(MOV, mI):
  
    display_read(addrA);
    
    //privilege conservation for biotic execution  
    if (IR_ptr->tag) {
      ++qcw->C_F; 
   
      --qcw->bP; // reflects qcw->molecules[*TAGed].P_value  
      --IR_ptr->P_value;
      
      //src biotic and dst abiotic 
      if (!addrB_ptr->tag && memory[addrA].tag) {
	--qcw->C_F;

	++qcw->tP; // reflects qcw->molecules[*].tag 
	addrB_ptr->tag = memory[addrA].tag;
	
      }
      else if (addrB_ptr->tag && !memory[addrA].tag) {
	++qcw->C_F; 

	--qcw->tP; // reflects qcw->molecules[*].tag 
	addrB_ptr->tag = memory[addrA].tag;
 
      }      
      else 
	addrB_ptr->tag = memory[addrA].tag;
    }
    
    addrB_ptr->opcode = memory[addrA].opcode;
    addrB_ptr->A_mode = memory[addrA].A_mode;
    addrB_ptr->B_mode = memory[addrA].B_mode;
    addrB_ptr->A_value = AA_Value;
    addrB_ptr->B_value = IR.A_value;

    //display_write(addrB); 

#ifndef SERVER    
    if (copyDebugInfo) 
      addrB_ptr->debuginfo = memory[addrA].debuginfo;
#endif
    
  } /* end switch */
	
  /* push the next instruction onto queue */
  if ((temp=progCnt + 1) == qcw->coreSize)
    temp = 0;
  return (temp); 
  
 skip:
  ADDMOD(progCnt, 2, temp);
  return (temp); 
  
}
