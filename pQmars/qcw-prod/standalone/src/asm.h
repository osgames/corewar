/* pQmars -- a Portable Quantum Memory Array Redcode Simulator
 *
 * Copyright (C) 2003-2004 Alexander (Sasha) Wait and others
 *                          --please see AUTHORS for details 
 *                         
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307,  USA.  
 *
 */


#define NONE      0
#define ADDRTOKEN 1
#define FSEPTOKEN 2
#define COMMTOKEN 3
#define MODFTOKEN 4
#define EXPRTOKEN 5
#define WSPCTOKEN 6
#define CHARTOKEN 7
#define NUMBTOKEN 8
#define APNDTOKEN 9
#define MISCTOKEN 10                /* unrecognized token */

#define sep_sym ','
#define com_sym ';'
#define mod_sym '.'
#define cat_sym '&'
#define cln_sym ':'

extern char addr_sym[], expr_sym[], spc_sym[];
extern char *opname[], *modname[];

extern char *pstrdup(char *), *pstrcat(char *, char *), *pstrchr(char *, int);
extern unsigned char ch_in_set(unsigned short, char *), skip_space(char *, unsigned short);
extern unsigned char str_in_set(char *, char *s[]);
extern int get_token(char *, unsigned char *, char *);
extern void to_upper(char *);

