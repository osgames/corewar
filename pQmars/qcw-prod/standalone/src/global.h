/* pQmars -- a Portable Quantum Memory Array Redcode Simulator
 *
 * Copyright (C) 2003-2004 Alexander (Sasha) Wait and others
 *                          --please see AUTHORS for details 
 *                         
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307,  USA.  
 *
 */

#include "config.h"

#include <stdio.h>
#ifndef INT_MAX
#include <limits.h>
#endif

#if defined(__STDC__) 
#include <stdlib.h>
#endif

#include "rand.h"

/* *********************************************************************
   System dependent definitions or declarations
   ********************************************************************* 
 */

// ASW -- see http://www.gnu.org/prep/standards_toc.html

#if !defined(MALLOC)

#define MALLOC(x)     malloc((size_t)(x))
#define REALLOC(x, y) realloc((void *)(x), (size_t)(y))
#define FREE(x)       free((void *)(x))

#endif                                /* ! MALLOC */


/* FALSE, TRUE */
#ifndef TRUE
enum {
  FALSE, TRUE
};
#endif

/* global error output macro */
#define errout(s) fputs(s,stderr)


/* ************************************************************************
   pmars global structures and definitions
   ************************************************************************ */

/* Version and date */

#define PQMARSVER  100
#define PQMARSDATE "2004/05/20"


/* return code:
   0: success
   Negative: System error such as insufficient space, etc
   Positive: User error such as number too big, file not found, etc 
*/
#define GRAPHERR    -3        // graphic error 
#define MEMERR      -2        // insufficient memory, cannot free, etc. 
#define INTERNALERR -1        // program logic error
#define FNOFOUND     1        // File not found 
#define CLP_NOGOOD   2        // command line argument error
#define PARSEERR     3        // File doesn't assemble correctly 
#define USERABORT    4        // user stopped program from cdb, etc. 
#define QCWFERR      5        // Error reading QCW file 
 

/* these are used as return codes of internal functions */
#define SUCCESS    0
#define WARNING    0

/* used by eval.c */
#define OVERFLOW  1
#define OK_EXPR   0
#define BAD_EXPR -1
#define DIV_ZERO -2

/* used by cdb.c */
#define NOBREAK 0
#define BREAK   1
#define STEP    2

#define SKIP    1                /* cmdMod settings for cdb: skip next command */
#define RESET   2                /* clear command queue */

#define UNSHARED -1                /* P-space is private */
#define PIN_APPEARED -2

/* used by sim.c and asm.c */

#define INDIR_A(x) (0x80 & (x))
#define RAW_MODE(x) (0x7F & (x))
#define SYM_TO_INDIR_A(x) (((x)-3)|0x80)  // turns index into addr_sym[]
                                          // to INDIR_A code 
#define INDIR_A_TO_SYM(x) (RAW_MODE(x)+3) // vice versa 



/* used by many */

// remember to be careful about the type of constant "1" 
#define POWER_OF_TWO(x) (1 << (x))  

#define STDOUT stdout

#define MAXWARRIOR        36

#define MAXINSTR         16000

#define MAXALLCHAR 256

#define MAXN 5000

/* The following holds the order in which opcodes, modifiers, and addr_modes
   are represented as in parser. The enumerated field should start from zero */
enum addr_mode {
  IMMEDIATE,                     /* # */
  DIRECT,                        /* $ */
  INDIRECT,                      /* @ */
  PREDECR,                       /* < */
  POSTINC                        /* > */
};


enum op {
  MOV, ADD, SUB, MUL, DIV, MOD, JMZ,
  JMN, DJN, CMP, SLT, SPL, DAT, JMP,
  QOP, QCN, IJN, SEQ, SNE, NOP
};                                   /* has to match asm.c:opname[] */

enum modifier {
  mA,                                /* .A  */
  mAd, 
  mAe,                              
  mAde, 

  mB,                                /* .B  */
  mBd, 
  mBe,                              
  mBde, 

  mAB,                               /* .AB */
  mABd, 
  mABe,                             
  mABde, 

  mBA,                               /* .BA */
  mBAd, 
  mBAe,                           
  mBAde, 

  mX,                                /* .X  */
  mXd, 
  mXe,                              
  mXde, 

  mF,                                /* .F  */
  mFd, 
  mFe,                         
  mFde, 

  mI,                                /* .I  */ 
  mId,
  mIe,                         
  mIde,

  mP,                                /* .P  */ 
  mPd, 
  mPe,
  mPde, 

  mQ,                                /* .Q  */ 
  mQd, 
  mQe,
  mQde 
}; 



typedef int ADDR_T;
#define ISNEG(x) ((x)<0)


//typedef unsigned char FIELD_T;
typedef unsigned short FIELD_T;

typedef unsigned long U32_T;        // unsigned long (32 bits) 
typedef long S32_T;

typedef unsigned long long U64_T;   // unsigned long long (64 bits) 
typedef long long S64_T;




/* Tag structure */

typedef struct tag_struct {

  U64_T md5high;
  U64_T md5low; 

  struct tag_struct *next; 

}tag_struct; 


/* Genotype Molecules ("backbone") */

typedef struct gen_mol_struct { 

  FIELD_T opcode, A_mode, B_mode;
  tag_struct *tag;
}gen_mol_struct; 


/* Genotype structure */

typedef struct gen_struct {

  U64_T md5high;
  U64_T md5low; 

  struct gen_struct *next; 
  
  long long freq; 
  gen_mol_struct *backbone; 
  long length; 

}gen_struct; 


/* Molecule structure */

typedef struct mem_struct {
  
  FIELD_T opcode, A_mode, B_mode;  

  ADDR_T A_value, B_value;

  S64_T P_value; 

  U32_T Q_value:1;
  U32_T Q_alloc:1;  
  U32_T Q_index:5;  // max 2^5 qubits per block 
  U32_T Q_block:25; // max 2^25 blocks     

  tag_struct *tag; 

  char debuginfo;

}mem_struct;

// ASW -- must match number of bits in Q_index 
#define QBLOCKWIDTH 12

// ASW -- yet another arbitrary choice 
#define MAXQBLOCK 10000


/* Warrior structure */
typedef struct warrior_struct {
  ADDR_T *taskHead, *taskTail;
  int     tasks;
  ADDR_T  lastResult;
  ADDR_T  position;         /* load position in core */
  int     startCore;        /* ASW -- start in this core */  
  int     instLen;          /* Length of instBank */
  int     offset;           /* Offset value specified by 'ORG' or 'END'.
                             * 0 is default */

  char   *name;             /* warrior name */
  char   *version;
  char   *date;
  char   *standard; 
  char   *fileName;         /* file name */
  char   *authorName;       /* author name */ 
  mem_struct *instBank;

  struct warrior_struct *next;

}warrior_struct; 


/* Core structure */ 

typedef struct core_struct { 
  int probC;                // prob. of clearing core [1/probC] 
  int probP;                // prob. of new privilege in core [1/probP]

  mem_struct *memory;       // pointer into world_struct.memory 

  warrior_struct *thread94; // '94 style thread array 
                            //   (size world_struct.maxThreads94)
}core_struct; 



/* Film structure 

typedef struct film_struct{
  mem_struct **priv_last_slice;    
  mem_struct **priv_last_new_slice; 
  mem_struct **active_mol;  
  long P; 
  int size; 
  
  core_struct *core;  
  }film_struct; 

*/


/* World constants for all simulations */ 

#define def_2_to_W POWER_OF_TWO (def_W) 

#define def_pi 1 
#define def_sigma  0 
#define def_psi    12  

#define min_C 8 

#define min_XYZ 2 
#define max_XYZ 4


#define min_2_to_XYZ 4 
#define max_2_to_XYZ 16

#define min_M 1 
#define max_M 5 

#define def_W 14

#define def_X 2
#define def_Y 2
#define def_Z 2
#define def_M 1 

#define def_F 0


typedef struct world_struct{

  // ----------------------------------------------------------------

  // World state at time t, World(t), is given by:  
  // 
  // *** World(t) = Molecules(t) + Climate(t) + Q.Histories(t) ***
  //
  // Time is measured from the beginning of an Epoch, t=0, to its end
  // at time t=T1.  At the beginning of each Epoch "Interventions" can, 
  // optionally, disturb the Quantum Coreworld from the "outside".  
  //         
  // The unit of time is the "cycle".
  //
  //                                 deterministically
  //  
  // *** World(0) [+ Intervention]         ===>          World(T1) ***  
  //
  // 
  // A non-trivial property of World(T1) is the function: 
  // 
  //                        T 1
  // *** F(g,x,y,z,T1)  =  S U M  f(g,x,y,z,t) 
  //                        t=0
  // 
  // where f(g,x,y,z,t) is the number of occurrences of Genotype g 
  // in Core (x,y,z) at time t; similarily, p(g,x,y,z,t) is the 
  // "(total) privilege of Genotype g in Core (x,y,z) at time t. 
  // 
  // The four dimensional matrix of all values of F(g,x,y,z,T1) for a 
  // given value of T1 and all combinations of g,x,y,z is referred to 
  // as F at time T1 or F(T1); f(T1), p(T1) are defined in the same way. 
  //                                
  // A Quantum Coreworld simulator must be able to:
  //
  //   1) load World(0) and (optionally) an Intervention 
  //
  //   2) deterministically compute the new state (at time t=t+1) 
  //        until the epoch's end (at time t=T1) 
  //
  //   3) save World(T1) 
  //   
  //   4) report F(T1), f(T1) and p(T1).  
  //
  //      N.B. F(T1) cannot be computed from World(T1) 
  //           it must be computed from World(0)+(Intervention)  
  //        
  //      f(T1), p(T1) can be computed from World(T1) directly  
  //   
  // Commmand line: 
  // 
  //   bzcat 0.qcw.gz.bz2 | zcat | ./pQmars -@ - [I1.red ...] | 
  //     gzip | bzip2 >1.qcw.gz.bz2
  //


  // ---------------------------------------------------------------
  // 
  // (C)limate: W X Y Z C M F 
  // 
  long C_W;              // C_W = C_X+C_Y+C_Z+C_C 

  long C_X, C_Y, C_Z;    // dim X = 2^C_X, dim Y = 2^C_Y, dim Z = 2^C_Z 
                         //

  long C_C;              // C_C = C_W - (C_X+C_Y+C_Z); 

  long C_M;              // multiplier = 2^C_M-1 
                         //

  // ---------------------------------------------------------------
  //
  // (P)rivilege: 
  //
  // Conservation of privilege in the Coreworld: 
  // 
  // P0 = tP0 + bP0 + fP0 (initial privilege) 
  // 
  // *** tP + bP + fP + cP == P0 + nP **** 
  // 
  // privilege can be moved around between "accounts" on the left 
  // side but can never be "created" or "destroyed" except through the 
  // "nP" account on the right.  When new privilege enters the world 
  // through the "nP" account some "accounts" on the left side must also 
  // reflect this new privilege.  "shadow" accounts track privilege in
  // qcw->molecules; every change to qcw->molecules should be reflected 
  // in these accounts.  

  long long tP0, bP0, C_F0;    // tag, bound, free            (initial) 
  long long tP,  bP,  C_F;     // tag, bound, free              
  long long cP,  nP;           // consumed, new            

  
  
  // ------------------------------------------------------------------
  //
  // (R)andomness:  
  //

  randctx *ctx;        // ISAAC random context 

  // ----------------------------------------------------------------- 
  //
  // (M)olecules:  
  //

  mem_struct *molecules; // must allocate 2^W "molecules", uses "tags"
  
  tag_struct *tags;      // linked list of tags for biotic molecules

  gen_struct *genotypes; // linked list of genotypes (in metaworld)

  // ---------------------------------------------------------------
  //
  // (Q)_Histories:  
  //

  // ASW -- To do!  

  // ----------------------------------------------------------------

  long taskMax;          // maximum abiotic tasks (per core) 

  long long steps, execs, e_steps, e_execs; 
  long long cycles; // from begining of time 

  mem_struct last_molecule; 

  tag_struct *intervention_tag;  // last intervention or NULL

  // Derived constants (and book-keeping) 

  long probMask;         // 2^(W-X-Y)-1  
  long XYZ;              // X + Y + Z 
  long dimX, dimY, dimZ; // dimX = 2^X, dimY = 2^Y, dimZ = 2^Z
  long dimXYZ_min_1;     // dimX*dimY*dimZ - 1;          

  long C_T;            // time to end of epoch
                                                                        
  // new privilege (p) in terms of (z) position:   
  // p = b*(multiplier-1)/(2^Z-1) * z + b where 
  //                      
  long b;                // b = 2^[(C_W-C_X-C_Y+1)/C_M)]  

  long coreSize;         // coreSize = 2^C 


  long quadSize;         // quadSize = coreSize / 4 

  int init;              // world (already) initialized [0 or 1] 

  // deprecated (but still used)... 
  ADDR_T coreNorth, coreEast, coreSouth, coreWest; 

  // needed by cdb 
  warrior_struct def_warrior; 
  ADDR_T def_task; 

  // Derived variables (and book-keeping) 

  mem_struct **priv_last_slice;    
  mem_struct **priv_last_new_slice; 
  mem_struct **active_mol;    

  core_struct *cores; 

  long programs, seed; 

}world_struct; 


/* ********************************************************************
   pmars global variable declarations
   ******************************************************************** */

extern int errorcode;
extern int errorlevel;
extern char errmsg[MAXALLCHAR];

/* Some parameters */

extern world_struct *qcw;  

// deprecated 

extern int curN;  // ASW -- film size s    
extern int curN2; // 
extern int maxN;  // 1 <= maxN <= MAXN  also see clparse.c 
extern int maxN2;
extern int minN;  // ASW -- this is a serious mess. 
extern int minN2; 
extern int swapN;
extern int swapN2; 
extern int curFilm;
extern int maxFilm; 

extern int cmdMod;

extern int SWITCH_d; 
extern int SWITCH_v; 

extern int inCdb;
extern int debugState;
extern int copyDebugInfo;

#if defined(XWINGRAPHX) || defined(SDLGRAPHX) 
extern int inputRedirection;
#endif

#if defined(XWINGRAPHX)
extern int xWinArgc;
extern char **xWinArgv;
#endif

extern warrior_struct *warrior; 


/* ***********************************************************************
   display define's, declarations and typedefs
   *********************************************************************** */

#if defined(XWINGRAPHX) || defined(SDLGRAPHX) 

#define GRX 1
#define SPEEDLEVELS 9
#define NORMAL_ATTR 0x0700
extern int displayLevel;
extern int displayMode;
extern int displaySpeed;
extern int SWITCH_g;
extern int refreshInterval;
extern int refIvalAr[SPEEDLEVELS];
extern int keyDelay;
extern int keyDelayAr[SPEEDLEVELS];
extern unsigned long loopDelay;
extern unsigned long loopDelayAr[SPEEDLEVELS];
#endif                            


/* ***********************************************************************
   function prototypes
   *********************************************************************** */

extern char *cellview(mem_struct * cell, char *outp, int emptyDisp);

extern void write_world_climate (FILE *, unsigned long long *, 
				 unsigned long long *);

extern void write_world_randomness (FILE *, unsigned long long *, 
			       unsigned long long *);

extern void write_world_molecules (FILE *, unsigned long long *, 
				   unsigned long long *);

extern void write_intervention (FILE *, unsigned long long *, 
				unsigned long long *); 

extern void write_genotype (mem_struct *, mem_struct *, FILE *, 
			    unsigned long long *, unsigned long long *); 


extern void read_qcw_file (FILE *filep);

extern void read_write_meta_file (FILE *filep);


extern int is_empty_molecule (mem_struct * molecule); 

extern void set_empty_molecule (mem_struct * molecule);
extern void set_empty_molecule_adj_priv (mem_struct * molecule);

extern void begin_world (int argc, char **argv); 
extern void do_intervention (void); 
extern long do_new_privilege (warrior_struct *); 
extern void do_exchange (void); 
extern void do_clear (void); 
extern void do_2004_exec (void);
extern void do_1994_exec (void);  
extern void end_world (void); 

extern void merge_films (void);

extern void add_genotype (mem_struct *, mem_struct *); 

extern int exec_cycle (mem_struct *IR); 

extern tag_struct * new_tag (unsigned long long, unsigned long long, 
		      int *);

extern gen_struct * new_genotype (unsigned long long, unsigned long long,
			   int *);


extern void molecule_to_cartesian (mem_struct *molecule, 
			    int *x, int *y, int *z, int *c);


extern mem_struct * cartesian_to_molecule (int x, int y, int z, int c );

extern int parse_param(int argc, char *argv[]);
extern int eval_expr(char *expr, long *result);
extern int assemble(char *fName, int aWarrior);
extern void disasm(mem_struct * cells, ADDR_T n);
extern void simulator1(void);
extern char *locview(ADDR_T loc, char *outp);
extern int cdb(char *msg);
extern int score(int warnum);
extern void sort_by_score(int *idxV, int *scrV);
extern int deaths(int warnum);
extern void results(FILE * outp);
extern void sort_by_score();
extern void Exit(int code);
extern void reset_regs(void);
extern void set_reg(char regChr, long val);

#if defined(XWINGRAPHX) || defined(SDLGRAPHX) 
extern void decode_vopt(int option);
extern void grputs(char *str);
extern void aputs5(char *str, int attribute);

#if defined(XWINGRAPHX)
extern void xWin_puts(char *s);
extern void xWin_write_menu(void);
extern void xWin_clear(void);
extern void xWin_update(int curPanel);
extern void xWin_display_close(int wait);
extern char *xWin_gets(char *s, int maxstr);
extern void xWin_resize(void);
#endif

#if defined(SDLGRAPHX)
void  sdlgr_clear_arena (void);
void  sdlgr_refresh (int what);	/* -1: all, 0: core, >0 all text panels. */
void  sdlgr_relayout ();
void  sdlgr_write_menu (void);
void  sdlgr_update (int nextpanel);
void  sdlgr_clear (void);
void  sdlgr_puts (const char *s);
char *sdlgr_gets (char *buf, int maxbuf, const char *prompt);
void  sdlgr_display_clear (void);
void  sdlgr_open_graphics (void);
void  sdlgr_display_close (int wait);
int   sdlgr_text_lines (void);
void  sdlgr_set_displayLevel(int level);
void  sdlgr_set_displaySpeed(int speed);
void  sdlgr_set_displayMode(int mode);
#endif


#endif


