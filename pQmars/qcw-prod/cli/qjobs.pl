#! /usr/bin/perl  -w

#don't buffer STDOUT or STDERR; from perl cookbook recipe 7.12 
select((select(STDOUT), $| = 1)[0]);
select((select(STDERR), $| = 1)[0]);

@nodes =
 ( 1,  2,  3,  4,  5,  6,  7,  8,  9, 10,
  11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
  21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
  31, 32, 33, 34, 35); 

foreach $i (@nodes) {

  $node = sprintf("node0%2.2d",$i); 

  open(NODE, "nc ".$node." 22 -zvw 1 2>&1 |"); 	
  chomp ($node_nc = <NODE>); 
  close (NODE); 

  if ($node_nc =~ m/open$/) {

    open (NODE, "rsh ".$node." w 2>&1 |");
    chomp ($node_w = <NODE>);
    close (NODE);

    if ($node_w =~ m/load average: (\d+)/) {
      print "$1 $node$node_w\n";

      open (NODE, "rsh $node ps aux 2>&1 |");
    
      while (<NODE>) { 
        chomp;	 
        if ($_ =~ m/(await|scifi|scifi2)[ ]+(\d+)/ && $_ !~ m/ps aux$/ ) {
          print "$node $_\n";
        } 
      }
      close (NODE); 
    }
  } 
}

