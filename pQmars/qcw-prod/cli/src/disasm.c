/* pQmars -- a Portable Quantum Memory Array Redcode Simulator
 *
 * Copyright (C) 2003-2004 Alexander (Sasha) Wait and others
 *                          --please see AUTHORS for details 
 *                         
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307,  USA.  
 *
 */

/* convert internal state to printable strings 
 * output internal state as printable strings 
 * compute (externally verifiable) md5sums of internal state 
 *
 * also see asm.c 
 */


#include "global.h"
#include "asm.h"
#include "sim.h"
#include "md5.h"
#include "rand.h"
#include <string.h>

#define denormalize(x) \
    ((int) ((x) > qcw->coreSize / 2 ? ((int) (x) - qcw->coreSize) : (int) (x)))

/* whether INITIALINST is displayed or not */

extern char *info01;

#define HIDE 0
#define SHOW 1


/* ******************************************************************** */

char   * cellview(mem_struct *cell, char *outp, int emptyDisp)
{
  FIELD_T opcode, modifier;
  char P_string[MAXALLCHAR]; 

  opcode = ((FIELD_T) (cell->opcode & 0xff00)) >> 8;
  modifier = (cell->opcode & 0xff);
  
  if ((emptyDisp == SHOW) || !is_empty_molecule (cell) ) { 
    
    sprintf(P_string, ", p %lld", cell->P_value); 
    
    sprintf(outp, "%3s.%-4s %c%8d, %c%8d%s%s%s%s",
	    opname[opcode],
	    modname[modifier],
            INDIR_A(cell->A_mode) ? 
	    addr_sym[INDIR_A_TO_SYM(cell->A_mode)] : addr_sym[cell->A_mode],
	    denormalize(cell->A_value),
            INDIR_A(cell->B_mode) ? 
	    addr_sym[INDIR_A_TO_SYM(cell->B_mode)] : addr_sym[cell->B_mode],
	    denormalize(cell->B_value),
	    cell->P_value ? P_string : "", 
	    cell->Q_alloc ? " Q" : (cell->Q_value ? " 1" : ""),
	    cell->tag ? " B" : "",
#ifdef SERVER
	    "");
#else
            cell->debuginfo ? " T" : "" );
#endif
  }
  else
    *outp = 0;

  return (outp);
}

/* ******************************************************************** */

char   *
locview(loc, outp)
  ADDR_T  loc;
  char   *outp;
{
  char    buf[MAXALLCHAR];
  sprintf(outp, "%05d %05d   %s\n", curCore, loc, cellview(memory + loc, buf, HIDE));
  return (outp);
}


void write_intervention  (FILE *print, unsigned long long *md5high, 
                           unsigned long long *md5low) 
{
  char buf[MAXALLCHAR], buf_cell[MAXALLCHAR];
  md5_state_t state; 
  int i, j;
  
  md5_init(&state); 

  for (i = 0; i < qcw->programs; i++) {
    
    if (print) {
      fprintf(print, info01, warrior[i].name, warrior[i].instLen,
	      warrior[i].authorName, warrior[i].standard);
    }
    for (j = 0; j < warrior[i].instLen; j++) {
      sprintf(buf, "I %s", 
	      cellview( warrior[i].instBank + j, buf_cell, TRUE));
      
      //molecules are not properly tagged until "placed"... 
      if (!strcmp (warrior[i].standard, "2004"))
	strcat (buf, " B");
      else if (strcmp (warrior[i].standard, "1994"))
	Exit(INTERNALERR);
   
      if (j == warrior[i].offset) 
	strcat (buf, " START\n"); 
      else
	strcat (buf, "\n");
      
      if (print)
	fprintf(print, "%s", buf);
	
      md5_append (&state, (md5_byte_t *) buf, strlen(buf)); 
    }   
  }
  md5_longlong(&state, md5high, md5low); 
}

void write_genotype (mem_struct *gen_begin, mem_struct *gen_end, 
		     FILE *print, 
		     unsigned long long *md5high, 
		     unsigned long long *md5low)
{
  mem_struct *molecule;
  md5_state_t state;
  char buf[MAXALLCHAR];

  FIELD_T opcode, modifier;


  md5_init(&state); 

  for (molecule = gen_begin; molecule < gen_end; molecule++) {

    opcode = ((FIELD_T) (molecule->opcode & 0xff00)) >> 8;
    modifier = (molecule->opcode & 0xff);

    sprintf (buf, "G %s.%s%c%d%c%d\n", 
	     opname[opcode], 
	     modname[modifier], 
	     INDIR_A(molecule->A_mode) ? 
	     addr_sym[INDIR_A_TO_SYM(molecule->A_mode)] : 
	     addr_sym[molecule->A_mode],
	     denormalize(molecule->A_value),
	     INDIR_A(molecule->B_mode) ? 
	     addr_sym[INDIR_A_TO_SYM(molecule->B_mode)] : 
	     addr_sym[molecule->B_mode],
	     denormalize(molecule->B_value));
    
    md5_append(&state, (md5_byte_t *) buf, strlen (buf));
    
    if (print)
      fprintf(print, "%s", buf);
  }
  md5_longlong(&state, md5high, md5low);
  
}


void write_world_climate  (FILE *print, unsigned long long *md5high, 
			   unsigned long long *md5low) 
{
  char buf[MAXALLCHAR];
  md5_state_t state; 

  sprintf(buf,
   "C W %ld\nC X %ld\nC Y %ld\nC Z %ld\nC M %ld\nC F %lld\n", 
   qcw->C_W,qcw->C_X,qcw->C_Y,qcw->C_Z,qcw->C_M,qcw->C_F);
  
  if (print)
    fprintf(print, "%s", buf); 

  md5_init (&state); 
  md5_append(&state, (md5_byte_t *) buf, strlen (buf)); 
  md5_longlong (&state, md5high, md5low); 
  
}

void write_world_randomness (FILE *print, unsigned long long *md5high, 
			     unsigned long long *md5low) 
{
  char buf[MAXALLCHAR];
  md5_state_t state; 
  int i; 

  md5_init (&state); 
  
  sprintf(buf, "R %08x%08x\nR %08x%08x\n", 
   (unsigned int) qcw->ctx->randcnt, (unsigned int) qcw->ctx->randa, 
   (unsigned int) qcw->ctx->randb, (unsigned int) qcw->ctx->randc);
  
  md5_append (&state, (md5_byte_t *) buf, strlen(buf));
  
  if (print)
    fprintf(print, "%s", buf); 
  
  for ( i = 0; i < RANDSIZ; i++) {
    
    sprintf (buf, "R %08x%08x\n", 
	     (unsigned int) qcw->ctx->randrsl[i],
	     (unsigned int) qcw->ctx->randmem[i]); 

    md5_append (&state, (md5_byte_t *) buf, strlen(buf));

    if (print)
      fprintf(print, "%s", buf);
  }   
  md5_longlong (&state, md5high, md5low);   
}



void write_world_molecules (FILE *print, unsigned long long *md5high, 
			    unsigned long long *md5low)
{
  mem_struct *molecule;
  char buf[MAXALLCHAR], tag_buf[MAXALLCHAR];
  md5_state_t state;

  md5_init(&state); 

  for (molecule = qcw->molecules; 
       molecule < qcw->molecules+def_2_to_W; 
       molecule++) {   
  
    if (!is_empty_molecule (molecule)) {

      if (molecule->tag) {
	sprintf(tag_buf, "%016llx%016llx", 
		molecule->tag->md5high, molecule->tag->md5low);   
      }
      else {
	fprintf(stderr, "tag ID NULL!\n"); 
      }

      if (strlen(tag_buf) != 32 ) {
	fprintf(stderr, "tag problems!\n"); 
	Exit(INTERNALERR); 

      }


      sprintf(buf, "M %04x%02x%02x%s %x %x %llx %d\n",
	      molecule->opcode, 
	      molecule->A_mode,
	      molecule->B_mode, 
	      tag_buf,
	      molecule->A_value,
	      molecule->B_value, 
	      molecule->P_value, 
	      molecule->Q_alloc ? -1 : (molecule->Q_value ? 1 : 0));
    }
    else 
      sprintf(buf, "M\n"); 

    //hash as we go 
    md5_append (&state, (md5_byte_t *) buf, strlen(buf));
    
    if (print)
      fprintf(print, "%s", buf); 
  } 
  md5_longlong(&state, md5high, md5low);    
}




