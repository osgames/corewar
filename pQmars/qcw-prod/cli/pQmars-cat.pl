#! /usr/bin/perl

#don't buffer STDOUT or STDERR; from perl cookbook recipe 7.12 
select((select(STDOUT), $| = 1)[0]);
select((select(STDERR), $| = 1)[0]);

$user = $ARGV[0]; 
$node = $ARGV[1]; 

$command = 
    "nice -n 19 bzcat |". 
    "nice -n 19 zcat |".
    "nice -n 19 /home/await/bin/pQmars-scifi -@ - -@ - |". 
    "nice -n 19 gzip |".
    "nice -n 19 bzip2";

print "$user $node $command\n"; 

#system($command) && die; 
