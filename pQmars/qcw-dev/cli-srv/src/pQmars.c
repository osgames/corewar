/* pQmars -- a Portable Quantum Memory Array Redcode Simulator
 *
 * Copyright (C) 2003-2004 Alexander (Sasha) Wait and others
 *                          --please see AUTHORS for details 
 *                         
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307,  USA.  
 *
 */


/* ********************************************************
    pQmars.c: main(), toplevel, initialization, cleanup
   ******************************************************** */

#include <stdio.h>

#if defined(unix) || defined(SDLGRAPHX)
#include <signal.h>
#endif

#include "global.h"


#if defined(SDLGRAPHX)
// Renames main () for SDL...
#include "SDL.h"
#endif

#if defined(XWINGRAPHX) || defined(SDLGRAPHX)

#if defined(HAVE_UNISTD_H)
#include <unistd.h>
#endif

#endif

extern void results(FILE * outp);
void    Exit(int);
int     returninfo(void);



#if defined(XWINGRAPHX) || defined(SDLGRAPHX) 
void decode_vopt(int option) {
#if defined(XWINGRAPHX)
  extern int xDisplayType;        /* defined in xwindisp.c */

  xDisplayType = (option / 1000) % 10;
  option %= 1000;
#endif

  displayLevel = option % 10;
  displayMode = (option % 100 - displayLevel) / 10;
  displaySpeed = (option - displayMode * 10 - displayLevel) / 100;
  if (displaySpeed >= SPEEDLEVELS)
    displaySpeed = SPEEDLEVELS - 1;
  loopDelay = loopDelayAr[displaySpeed];
  keyDelay = keyDelayAr[displaySpeed];

#if defined(SDLGRAPHX)
  sdlgr_set_displayLevel(displayLevel);
  sdlgr_set_displaySpeed(displaySpeed);
  sdlgr_set_displayMode(displayMode);
#endif
}

#endif

/* called when ctrl-c is pressed; prepares for debugger entry */
#if defined(unix) || defined(SDLGRAPHX)
void sighandler(int dummy)
{
#ifndef SERVER
  debugState = STEP;
  cmdMod = RESET;                /* terminate any macros in process */
  /*
   * some machines (SPARCstation, RS6000) seem to lose the handler address
   * after first signal trap -> need to call signal() again
   */
#ifdef SIGINT
  signal(SIGINT, sighandler);
#endif
  
  Exit(USERABORT);
#endif
}
#endif                                /* unix */


int main(int argc, char  **argv)
{

  begin_world (argc, argv); 

  do_intervention (); 

  do { 
    
    do_2004_exec (); 
      
    //      if (qcw->abiotic) 
    //	do_1994_exec (); 
      
    //spike
    //if (!(rand(qcw->ctx) & (POWER_OF_TWO(qcw->C_C+qcw->C_Z)-1))) 
    //  qcw->nP += do_new_privilege (NULL);
    
    //bubble
    //if (!(rand(qcw->ctx) & (POWER_OF_TWO(qcw->C_C+qcw->C_Z)-1))) 
    //  do_exchange ();
      
    //wash
    //if (!(rand(qcw->ctx) & (POWER_OF_TWO(qcw->C_C+qcw->C_Z)-1))) 
    //  do_clear (); 
        
  } while (--qcw->C_T);

  end_world ();

  return (SUCCESS); 
}



/* Exit -- reports "errorcode" and exits
 *
 * begin_*, end_*, do_* and functions they call 
 * should use "Exit" on fatal error.
 */
void Exit(int errorcode) {
  exit(errorcode);
}
