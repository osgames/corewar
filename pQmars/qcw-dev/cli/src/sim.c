/* pQmars -- a Portable Quantum Memory Array Redcode Simulator
 *
 * Copyright (C) 2003-2004 Alexander (Sasha) Wait and others
 *                          --please see AUTHORS for details 
 *                         
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307,  USA.  
 *
 */


#include "global.h"
#include "sim.h"
#include "md5.h"

#include <string.h>
#include <time.h>

#ifdef unix 
#include <signal.h>
#endif 


#if defined(HAVE_UNISTD_H)
#include <unistd.h> //Needed for isatty 
#endif

#if defined(unix) || defined(SDLGRAPHX)
void    sighandler(int dummy);

#endif


#ifdef XWINGRAPHX
#include "xwindisp.h" // See also cycle.c 
#else
#ifdef SDLGRAPHX
#include "sdldisp.c"  // ditto 
#else                                /* !GRAPHX */
#define display_init()
#define display_clear()
#define display_read(addr)
#define display_write(addr)
#define display_dec(addr)
#define display_inc(addr)
#define display_exec(addr)
#define display_spl(warrior,tasks)
#define display_dat(address,warrior,tasks)
#define display_die(warnum)
#define display_close()
#define display_cycle()
#define display_push(val)
#endif                             
#endif 


/* strings */
extern char *outOfMemory;
extern char *warriorTerminated;
extern char *fatalErrorInSimulator;
extern char *warriorTerminatedEndOfRound;
extern char *endOfRound;



int curN, curN2;    //W -- need better support for multi-film
int maxN, maxN2; 
int minN, minN2;

int curFilm; 
int maxFilm; // max -small- films  

world_struct qcw_base; // allocate World
 
world_struct *qcw; //pointer to current world structure 
 
warrior_struct *multi_warrior[MAXN];  // Warriors
warrior_struct *swap_warrior; 

ADDR_T *multi_taskQueue[MAXN*MAXWARRIOR];
ADDR_T *swap_taskQueue;

mem_struct *multi_memory[MAXN];   /* ASW -- Core Array */ 

mem_struct *swap_memory; 

int curCore; 
int curQblock; 
int curQbit; 

warrior_struct *W;                /* running warrior */
warrior_struct *swap_W;           

U32_T   totaltask;                /* # tasks in core */
ADDR_T *endQueue;
ADDR_T *taskQueue;

mem_struct *memory;


ADDR_T  progCnt;      // ASW -- program counter(s)


/* ASW -- an unrelated variable of the same name (destPtr) is used 
   in addr. mode decode step in cycle.c */

mem_struct *destPtr;  // pointer used to copy program to core
                     

mem_struct ** sliceP; // priv. execution slice  

warrior_struct *endWar;     /* end warriors array */


/* Initialize world for the first time 
 * 
 */ 


void init_world (void) {
 
  int i; 
  
  qcw->ctx->randa=qcw->ctx->randb=qcw->ctx->randc=(ub4)0;
  
  if (!qcw->seed) 
    qcw->seed = time (0); 

  for (i=0; i<256; ++i) 
    qcw->ctx->randrsl[i]=(ub4)qcw->seed+i;    
  randinit(qcw->ctx, TRUE);
 
  fprintf (STDOUT, "$\n;QCW\n;SEED %ld\n", qcw->seed); 

  for (i = 0; i < def_2_to_W; i++) 
    set_empty_molecule (qcw->molecules+i);
  
  qcw->init = 1;
}




void begin_world (int argc, char **argv)
{
  ub4 i;

#if defined(unix) 
#ifdef SIGINT
  signal(SIGINT, sighandler);
#endif
#endif                                /* ifdef unix */
  
#if defined(XWINGRAPHX)
  xWinArgc = argc;
  xWinArgv = argv;
#endif

  qcw = &qcw_base;

  //no world loaded (yet) 
  qcw->init = 0; 
 
  //alocate memory 
  qcw->molecules = (mem_struct *) 
    malloc((size_t) def_2_to_W * sizeof(mem_struct));

  qcw->active_mol = (mem_struct **) 
    malloc((size_t) 10 * def_2_to_W * sizeof(mem_struct *));

  qcw->ctx = (randctx *) malloc((size_t) sizeof(randctx));
 
  if (!qcw->molecules || !qcw->active_mol || !qcw->ctx) 
    Exit(MEMERR); 

  qcw->taskMax = 64; // ASW -- set this somewhere else

  qcw->priv_last_slice = qcw->active_mol;
  qcw->priv_last_new_slice = qcw->active_mol;

  //default values for Climate 

  qcw->C_W = def_W; 
  qcw->C_X = def_X;
  qcw->C_Y = def_Y;
  qcw->C_Z = def_Z;
  qcw->C_M = def_M;
 
  qcw->C_F = def_F; 


  //parse simulator command line (and read from QCW file) 
  if ((errorcode = parse_param(argc, argv))) 
    Exit (errorcode); 

  if (!qcw->init) 
    init_world(); //QCW not loaded 

  // pQmars has initialized a new world or restored an old one...

  qcw->C_T = POWER_OF_TWO(qcw->C_W-1) + 
    ((POWER_OF_TWO(qcw->C_W)-1) & rand(qcw->ctx));
    
  qcw->XYZ = qcw->C_X+qcw->C_Y+qcw->C_Z; 

  qcw->dimX = POWER_OF_TWO(qcw->C_X); 
  qcw->dimY = POWER_OF_TWO(qcw->C_Y); 
  qcw->dimZ = POWER_OF_TWO(qcw->C_Z); 

  qcw->probMask = POWER_OF_TWO(qcw->C_W - qcw->C_X - qcw->C_Y) - 1; 

  qcw->dimXYZ_min_1 = POWER_OF_TWO(qcw->XYZ)-1; 

  qcw->C_C = qcw->C_W - qcw->XYZ;   

  qcw->coreSize = POWER_OF_TWO (qcw->C_W-(qcw->C_X+qcw->C_Y+qcw->C_Z)); 
 
  qcw->b = POWER_OF_TWO((qcw->C_W-qcw->C_X-qcw->C_Y+1)/qcw->C_M);
 
  qcw->quadSize = qcw->coreSize / 4; 

  //deprecated (but still used)...
  qcw->coreNorth = 0;
  qcw->coreEast = qcw->coreNorth + qcw->quadSize; 
  qcw->coreSouth = qcw->coreEast + qcw->quadSize;
  qcw->coreWest = qcw->coreSouth + qcw->quadSize; 

  //deprecated (but still used)... 
  minN = qcw->dimX; 
  maxN = qcw->dimY; 
  maxFilm = qcw->dimZ-1; 
  minN2 = minN * minN; 
  maxN2 = maxN * maxN;

  curN = maxN;
  curN2 = maxN2;   
  
  errorcode = SUCCESS;
  errorlevel = WARNING;                /* reserve for future */
  errmsg[0] = '\0';                /* reserve for future */
  
#if defined(XWINGRAPHX) || defined(SDLGRAPHX) 
  decode_vopt(SWITCH_g);
  if (!isatty(fileno(stdin)))
    //inputRedirection = TRUE;
#endif
 
  //(deprecated!?!) default warrior initialization for CDB
  qcw->def_warrior.taskHead = &qcw->def_task; 
  qcw->def_warrior.taskTail = &qcw->def_task;
  qcw->def_warrior.tasks = 0; // should be 1? 
  qcw->def_warrior.name = NULL; 
  qcw->def_warrior.next = &qcw->def_warrior; 
 
  curQblock = -1; 
  curQbit = QBLOCKWIDTH;    

}

void do_intervention (void) {

  int i, is_new; 
  long long md5high, md5low, newPrivilege;   
  warrior_struct *curWarrior; 
  mem_struct *molecule; 

  curCore = 0; // ASW -- eliminate dependence on curCore in display_init! 
  display_init(); 

  // ASW -- use ISAAC in libQuantum instead!  
  srandom(rand(qcw->ctx)); 

  newPrivilege = 0; 

  if (qcw->programs) {

    for (i = 0; i < qcw->programs; i++) {
      if (assemble(warrior[i].fileName, i))
	Exit(errorcode); 
      warrior[i].next = &warrior[i+1]; 
    }  
    warrior[i-1].next = &warrior[0]; 

    write_intervention (STDOUT, &md5high, &md5low); 
    
    qcw->intervention_tag = new_tag (md5high, md5low, &is_new);

    fprintf(STDOUT, ";MD5 %016llx%016llx I\n", md5high, md5low);
    
    curWarrior = warrior; 

    while (qcw->C_F > (qcw->coreSize * qcw->dimZ)) {
      qcw->C_F -= do_new_privilege(curWarrior);
      curWarrior = curWarrior->next;
    }
  }
  else { 
    while (qcw->C_F > (qcw->coreSize * qcw->dimZ)) { 
      qcw->C_F -= do_new_privilege (NULL); 
    }
  }
  
  qcw->tP0 = qcw->bP0 = 0; 

  qcw->C_F0 = qcw->C_F; 

  qcw->cP = qcw->nP = 0; 

  //set active molecules after intervention 
  for ( molecule = qcw->molecules; 
	molecule < qcw->molecules+def_2_to_W; 
	molecule ++ )  {
   
    if (molecule->tag) { 
      qcw->tP0++; 
      
      if (molecule->P_value) {
	*qcw->priv_last_slice++ = molecule;
	++qcw->priv_last_new_slice;
	qcw->bP0+= molecule->P_value; 
      }
    }
    else
      qcw->bP0+= molecule->P_value; 
  }
  qcw->tP = qcw->tP0; 
  qcw->bP = qcw->bP0;

  if (SWITCH_d)
    debugState = STEP;           // automatically enter debugger 
  
  if (!debugState)
    copyDebugInfo = TRUE;        // this makes things a little faster  

}



tag_struct * new_tag (unsigned long long md5high, 
		      unsigned long long md5low,
		      int *is_new) 
{
  tag_struct *tag, *last_tag; 

  if (!qcw->tags) {
    qcw->tags = (tag_struct *) malloc ((size_t) sizeof(tag_struct)); 
    if (!qcw->tags) 
      Exit(MEMERR); 
    qcw->tags->md5high = md5high;
    qcw->tags->md5low = md5low;
    qcw->tags->next = NULL; 
    *is_new = 1; 
    return (qcw->tags); 
  }
  
  tag = qcw->tags;   
  do {

    if (md5high == tag->md5high && md5low == tag->md5low) {
      *is_new = 0; 
      return (tag);  
    }
    last_tag = tag; 
    tag = tag->next;

  }while (tag); 
  
  tag = last_tag; 

  tag->next = (tag_struct *) malloc ((size_t) sizeof(tag_struct)); 
  if (!tag->next) 
    Exit(MEMERR); 

  tag->next->md5high = md5high;
  tag->next->md5low = md5low;
  tag->next->next = NULL;
  *is_new = 1; 
  return (tag->next); 
}

gen_struct * new_genotype (unsigned long long md5high, 
			   unsigned long long md5low, 
			   int *is_new) 
{
  gen_struct *genotype, *last_genotype; 

  if (!qcw->genotypes) {
    qcw->genotypes = (gen_struct *) malloc ((size_t) sizeof(gen_struct)); 
    if (!qcw->genotypes) 
      Exit(MEMERR); 
    qcw->genotypes->md5high = md5high;
    qcw->genotypes->md5low = md5low;
    qcw->genotypes->next = NULL; 
    qcw->genotypes->freq = 1; 
    *is_new = 1; 
    return (qcw->genotypes); 
  }
  
  genotype = qcw->genotypes; 
  
  do {
    
    if (md5high == genotype->md5high && md5low == genotype->md5low) {
      ++genotype->freq;
      *is_new = 0; 
      return (genotype);  
    }
    last_genotype = genotype; 
    genotype = genotype->next; 
    
   } while (genotype);
  
  genotype = last_genotype; 

  genotype->next = (gen_struct *) malloc ((size_t) sizeof(gen_struct)); 
  if (!genotype->next) 
    Exit(MEMERR); 

  genotype->next->md5high = md5high;
  genotype->next->md5low = md5low;
  genotype->next->next = NULL;
  genotype->next->freq = 1; 
  *is_new = 1; 
  return (genotype->next); 
}

int verify_shadow_privilege () {
  
  mem_struct * molecule; 
  long long tP, bP, verify;
  
  tP = bP = 0; 
  
  for ( molecule = qcw->molecules; 
	molecule < qcw->molecules+def_2_to_W; 
	molecule ++ )  {
    
    if (molecule->tag) 
      tP++; 
    
    bP+= molecule->P_value;      
  }  

  verify = 1; 

  if (qcw->tP != tP) {
    fprintf(stderr, "tP %lld != actual tP %lld\n", qcw->tP, tP); 
    verify = 0; 
  }  
  if (qcw->bP != bP) {
    fprintf(stderr, "bP %lld != actual bP %lld\n", qcw->bP, bP); 
    verify = 0; 
  }

  return (verify); 
}


void verify_privilege () {

  char outs[256];

  if ( qcw->tP  + qcw->bP  + qcw->C_F + qcw->cP - qcw->nP != 
       qcw->tP0 + qcw->bP0 + qcw->C_F0 ) {
    
#ifndef SERVER      
    debugState = BREAK;
    sprintf(outs, "Privilege conservation violated\n");
    debugState = cdb(outs);      
#else 

    fprintf(stderr, "Privilege conservation violated\n"); 
    
    fprintf(stderr, 
"tot%10lld = tP0%10lld +bP0%10lld +fP0%10lld\n",
	    qcw->tP0+qcw->bP0+qcw->C_F0, 
	    qcw->tP0, qcw->bP0, qcw->C_F0);
    
    fprintf(stderr,
"tot%10lld = tP %10lld +bP %10lld +fP %10lld\n",
	    qcw->tP+qcw->bP+qcw->C_F+qcw->cP-qcw->nP, 
	    qcw->tP, qcw->bP, qcw->C_F);   

    fprintf(stderr,
"               +cP %10lld -nP %10lld\n", qcw->cP, qcw->nP);
	            
    verify_shadow_privilege();
     
    fprintf(stderr, "steps %lld execs %lld e_steps %lld e_execs %lld\n",
	  qcw->steps, qcw->execs, qcw->e_steps, qcw->e_execs); 
    
    cellview( &qcw->last_molecule, outs, 1); 
    
    fprintf(stderr, "*** %s ***\n", outs); 
    
    fprintf(stderr, "%ld\n",qcw->C_T);  

    Exit(INTERNALERR); 
#endif 
  }
}


void end_world (void) {

unsigned long long md5high, md5low;

#ifndef SERVER
  char    outs[256];     
 
  if (debugState == BREAK) {
    sprintf(outs, endOfRound, 1);
    debugState = cdb(outs);
  }
  display_close(); 
#endif 

  //merge_films 
  merge_films();
     
  //output world (and signatures) 
  write_world_climate (STDOUT, &md5high, &md5low); 

  fprintf(STDOUT, ";MD5 %016llx%016llx C(1)\n", 
	  md5high, md5low);   
 
  write_world_randomness (STDOUT, &md5high, &md5low); 

  fprintf(STDOUT, ";MD5 %016llx%016llx R(1)\n", 
	  md5high, md5low);   

  write_world_molecules (STDOUT, &md5high, &md5low); 

  fprintf(STDOUT, ";MD5 %016llx%016llx M(1)\n", 
	  md5high, md5low); 

  //write_world_qhistories 
}


void merge_films (void) { 

  mem_struct * quad, * molecule;

  int quadSize;

  quadSize = POWER_OF_TWO (qcw->C_W-(qcw->C_X+qcw->C_Y+qcw->C_Z+2));

  for ( quad = qcw->molecules; 
	quad < qcw->molecules+def_2_to_W; 
	quad += quadSize) {

    set_empty_molecule_adj_priv (quad);
    
    set_empty_molecule_adj_priv (quad+quadSize-1);
  
  }
  for ( molecule = qcw->molecules; 
	molecule < qcw->molecules+def_2_to_W; 
	molecule ++ )  {

    if (!molecule->tag) 
      set_empty_molecule_adj_priv (molecule);    
  }
  if (!verify_shadow_privilege())
    Exit(INTERNALERR); 
}


void do_1994_exec (void) {

  int sliceI, curWarrior; 

  while (0) {

    /* ASW -- The switch to "films" of "compartments"  required
       tricking CDB to see only one core ("compartment") at a
       time.  Unfortunately-- with stochastic execution-- memory
       must also be reserved for task-queues one at a time (all
       warrior task-queues in different blocks of memory). For a
       small loss of functionality in CDB this solves
       queue-over-run problems. 
       
       All this changed with 2004 style execution. 	
       
    */
    
    curFilm = 0;

    for (sliceI = 0; sliceI<curN2; sliceI++) {
      
      for (curWarrior = 0; curWarrior < qcw->programs; curWarrior++) {
	
	/* **********************************************   
	   ASW -- change pointers/variables for current core */ 
	
	curCore = sliceI;
	
	if (curFilm) 
	  curCore += maxN2+minN2*(curFilm-1);
	
	warrior = multi_warrior[curCore];
	W = warrior+curWarrior;
	taskQueue = multi_taskQueue[curCore*qcw->programs+curWarrior];
	endQueue = taskQueue + totaltask; 
	
	memory = multi_memory[curCore];
	
	if (W->tasks) { 
	  display_cycle();
	  
	  progCnt= *(W->taskHead++);       
	  
	  if (W->taskHead == endQueue)
	    W->taskHead = taskQueue;
	  
#ifndef SERVER
	  if (debugState && ((debugState == STEP) || memory[progCnt].debuginfo))
	    debugState = cdb("");
#endif
	  
	  	  
	  if ( (memory[progCnt].opcode & 0xFF) != mP ) {
	    
	    if ((progCnt = exec_cycle(memory+progCnt)) != -1) {
	      *W->taskTail=progCnt; 
	      if (++W->taskTail== endQueue) 
		W->taskTail=taskQueue;
	      display_push(progCnt);
	    } 
	    
	  }
	  else {
	    //die if '94 style thread uses .P modifier 
	    --W->tasks; 
	  }
	}
      }
    }
  }
}

void do_2004_exec (void) { 

  long temp; 
 
  // do 2004 style execution 
  
  warrior = &qcw->def_warrior;
  W = warrior;
  W->tasks = 1; 
  taskQueue = &qcw->def_task;
  endQueue = taskQueue; 
  
  set_empty_molecule(&qcw->last_molecule);  

  for (sliceP=qcw->active_mol; 
       sliceP<qcw->priv_last_slice; 
       sliceP++) {
    
    // ASW -- SNIP -- 
    if ( !(*sliceP) || (*sliceP)->P_value < 0 ) { 
      printf("sliceP problems!\n");
      errout(outOfMemory);
      Exit(MEMERR);	  
    }
    // ASW -- SNIP --
    
    // no privilege?  abiotic?  then purge 
    if ( !(*sliceP)->P_value || !(*sliceP)->tag) {
      *sliceP = NULL;
      continue;
    }
    
    // execute or not?  
    if ( (*sliceP)->opcode & 2 ) {
      if (!(rand(qcw->ctx) & 3) ) {
	qcw->e_steps++; 
	continue; 
      }
      else {
	qcw->e_steps++; 
	qcw->e_execs++; 
      }
    }
    else if ((rand(qcw->ctx) & 3) ) { 
      qcw->steps++; 
      continue; 
    }
    else {
      qcw->steps++;
      qcw->execs++; 
    }   

    //if (!verify_shadow_privilege ()) 
    //  qcw->C_F0++; 
    verify_privilege();

    
    qcw->last_molecule = **sliceP;

      
    curCore = (*sliceP - qcw->molecules) >> qcw->C_C;    
    progCnt = (*sliceP - qcw->molecules) & (POWER_OF_TWO(qcw->C_C)-1);    
    memory = qcw->molecules+(curCore << qcw->C_C); 

    //deprecated (but used) curCore defined differently in other places
    curFilm = curCore & (qcw->dimZ-1);
    curCore = (curFilm << (qcw->C_X+qcw->C_Y))+(curCore >> qcw->C_Z);   
    
    //for CDB 
    *taskQueue = progCnt; 

    display_cycle();
    
#ifndef SERVER	 
    if (debugState && ((debugState==STEP)||memory[progCnt].debuginfo))
      debugState = cdb("");      
#endif
    if ((progCnt = exec_cycle(*sliceP)) != -1) {
      
      if (*sliceP ==  memory+progCnt)
	continue;      // same molecule active

      
      if ((*sliceP)->P_value > 0 ) { 	
	memory[progCnt].P_value += (*sliceP)->P_value;  
	(*sliceP)->P_value = 0;	    
      }
      else if ((*sliceP)->P_value == 0 ) {
        set_empty_molecule_adj_priv (*sliceP);  	
	--qcw->C_F; // freed at least one above (imp) 

	++memory[progCnt].P_value;  
	++qcw->bP; 
  
      }
      else if ((*sliceP)->P_value == -1) { 
        set_empty_molecule_adj_priv (*sliceP); 
        *sliceP = NULL;
	continue; 
      }
      else {
	printf("sliceP < -1 !\n");
	Exit(INTERNALERR); 
      }
    }    
    else 
      *sliceP = NULL; 
    
    *sliceP = memory+progCnt;       
    display_push(progCnt);
  }
  
  
  //clean-up inactive molecules
  
  sliceP=qcw->priv_last_new_slice;
  while (sliceP > qcw->active_mol) {
        
    if (!*(--sliceP))
      *sliceP = *(--qcw->priv_last_new_slice);
  } 
  
  // ready for next cycle 
  qcw->priv_last_slice = qcw->priv_last_new_slice;
  
  // finished 2004 style execution; 
}


int sum_flips (int num_flips) 
{
  int flips, sum;
 
  sum = 0; 
  for ( flips = 0; flips < num_flips; flips++) {    
    sum += (rand(qcw->ctx)&1);
  }  
  return (sum); 
}




long do_new_privilege (warrior_struct *curWarrior) {
  
  int i, x, y, z, c, q, p, skip; 
  mem_struct *molecule, *add_molecule;
 
  long long  prand, pfree, ptotal;

  x = sum_flips (POWER_OF_TWO(qcw->C_X)-1); 
  y = sum_flips (POWER_OF_TWO(qcw->C_Y)-1);
  c = rand(qcw->ctx) & (POWER_OF_TWO(qcw->C_C)-1);   
  q = c >> (qcw->C_C-2) << (qcw->C_C-2);  

  ptotal = 0; 

  skip = warrior ? 2 : 1; 
  
  for ( z = 0; z < POWER_OF_TWO(qcw->C_Z); z+=skip) { 
    
    p = POWER_OF_TWO(qcw->C_C); // use gradient(z)     

    prand = (p>>1) + rand (qcw->ctx) % p;
    pfree = 0; 

    //intervention 

    if (curWarrior) {    

      if (strcmp (curWarrior->standard, "2004") == 0 && 
	  prand > curWarrior->instLen) {
   
	molecule = cartesian_to_molecule (x, y, z, q);

	//printf("placing @ %d %d %d %d, ", x, y, z, q); 

	add_molecule = molecule + curWarrior->offset;
     	
	for (i = 0; i < curWarrior->instLen; i++) {

	  if (molecule->tag) {
	    pfree += 1 + molecule->P_value; 
	    qcw->tP--; 
	    qcw->bP-= molecule->P_value; 
	  }
	  else {
	    pfree += molecule->P_value; 
	    qcw->bP-=molecule->P_value; 
	  }
	  set_empty_molecule (molecule);	

	  *molecule = curWarrior->instBank[i];
	  molecule->tag = qcw->intervention_tag;
	  qcw->tP++; 

	  molecule++; 
	}
	add_molecule->P_value = prand - curWarrior->instLen;
	qcw->bP+= add_molecule->P_value; 
	
	//printf("prand %d, pfree %d\n", prand, pfree); 

	ptotal += prand-pfree; 
	prand = pfree;	
      }      	   
    }
    
    molecule = cartesian_to_molecule (x, y, z, c);

    //printf("privilege @ %d %d %d %d prand %d\n", x, y, z, c, prand); 
    
    if (!curWarrior && prand && !molecule->P_value && molecule->tag) {
      *qcw->priv_last_slice++ = molecule; 
      ++qcw->priv_last_new_slice; 
    }    
    molecule->P_value += prand;

    qcw->bP += prand; 
 
    ptotal += prand;    
  }  
  //printf ("slices %d\n",qcw->priv_last_slice - qcw->active_mol);   
  return (ptotal);
}



void do_exchange (void) {
  
  int x, y, z, c; 
  mem_struct *molecule1, *molecule2; 
  mem_struct moleculeT;  

  x = rand(qcw->ctx)&(POWER_OF_TWO(qcw->C_X)-1); 
  y = rand(qcw->ctx)&(POWER_OF_TWO(qcw->C_Y)-1); 

  for ( z = (rand(qcw->ctx)&1); z < POWER_OF_TWO(qcw->C_Z); z+=2) { 
    for ( c = 0; c < POWER_OF_TWO(qcw->C_C); c++) { 
      molecule1 = cartesian_to_molecule (x, y, z, c);
      molecule2 = cartesian_to_molecule (x, y, z+1, c);

      moleculeT = *molecule1; 
      *molecule1 = *molecule2; 
      *molecule2 = moleculeT; 

      //active molecules? 
      if (molecule1->P_value && !molecule2->P_value) {
	*qcw->priv_last_slice++ = molecule1;
	++qcw->priv_last_new_slice;
      }
      if (molecule2->P_value && !molecule1->P_value) {
	*qcw->priv_last_slice++ = molecule2;
	++qcw->priv_last_new_slice;
      }
    }    
  }   
}



void do_clear (void) {

  int x, y, z, c; 
 
  mem_struct *molecule; 
  
  x = (POWER_OF_TWO(qcw->C_X-1) + sum_flips (POWER_OF_TWO(qcw->C_X)-1)) 
    & (POWER_OF_TWO(qcw->C_X)-1); 
  y = (POWER_OF_TWO(qcw->C_Y-1) + sum_flips (POWER_OF_TWO(qcw->C_Y)-1)) 
    & (POWER_OF_TWO(qcw->C_Y)-1); 
  
  for ( z = 0; z < POWER_OF_TWO(qcw->C_Z); z++) { 
    for ( c = 0; c < POWER_OF_TWO(qcw->C_C); c++) { 

      molecule = cartesian_to_molecule (x, y, z, c); 
      set_empty_molecule_adj_priv (molecule);           
    }    
  }  
}


void add_genotype (mem_struct * gen_begin, mem_struct * gen_end) {

  unsigned long long md5high, md5low, md5high1, md5low1; 
  int is_new, x, y, z, c, i;  

  gen_struct *genotype; 

  write_genotype (gen_begin, gen_end, NULL, &md5high, &md5low); 

  genotype = new_genotype (md5high, md5low, &is_new); 

  if (is_new) {

    fprintf (STDOUT, "G MD5 %016llx%016llx\n",md5high,md5low);
    write_genotype (gen_begin, gen_end, STDOUT, &md5high1, &md5low1); 

    if (md5high != md5high1 || md5low != md5low) {
      fprintf(stderr, "Internal Error adding new genotype!\n");
      Exit (INTERNALERR); 
    }
    genotype->length = gen_end-gen_begin;

    genotype->backbone = (gen_mol_struct *) 
      malloc ((size_t) genotype->length * sizeof(gen_mol_struct));

    if (!genotype->backbone) 
      Exit(MEMERR); 

    for (i = 0; i < genotype->length; i++) {
      genotype->backbone[i].opcode = gen_begin[i].opcode; 
      genotype->backbone[i].A_mode = gen_begin[i].A_mode; 
      genotype->backbone[i].B_mode = gen_begin[i].B_mode; 
      genotype->backbone[i].tag = gen_begin[i].tag; 
    }
  }
  
  molecule_to_cartesian(gen_begin, &x, &y, &z, &c); 

  fprintf (STDOUT, ";G MD5 %016llx%016llx POS %08x %d %d %d %d\n",
	   md5high,md5low, gen_begin-qcw->molecules, x, y, z, c);

}

void read_write_meta_file (FILE *filep) {

  char buf[MAXALLCHAR]; 
  mem_struct * molecule, *gen_begin; 
  int world, instCount, i, count;
  int isRep, isRep1, isRep2, isCon, isQuant, isNone, checkProc, checkBound; 
  long long popTotal, popRep, popCon, popQuant, popNone;  
  long long instructions, instVerify, boundPrivilege, freePrivilege; 
  gen_struct * genotype; 
  int brick[2048]; 
  int bound[2048]; 

  do {
    fgets(buf, MAXALLCHAR, filep);
  }while (strncmp (buf, "$", 1));
  // should be ;QCW files from here ... 

  fgets (buf, MAXALLCHAR, filep); 
  //fprintf(stderr, "Read $ found %s", buf);

  world = 0; 
  instructions = 0; 
  boundPrivilege = 0; 
  freePrivilege = 0; 
  count = 0; 
  brick[0] = 0; 
  bound[0] = 0; 

  while (!strncmp (buf, ";QCW", 4)) {   
    // reading  ;QCW file
    read_qcw_file (filep);
     
    for (molecule = qcw->molecules; 
	 molecule < qcw->molecules+def_2_to_W; 
	 ++molecule) {
      count++;
      if ( !(count%32) ) {
	brick[count/32] = 0; 
	bound[count/32] = 0; 
      }
      if (!is_empty_molecule(molecule)) {
	brick[count/32]++; 
	bound[count/32]+= molecule->P_value; 
      }      
    }

    ++world; 
    freePrivilege += qcw->C_F; 

    for ( molecule = qcw->molecules; 
	  molecule < qcw->molecules+def_2_to_W; 
	  ++molecule) {

      gen_begin = molecule; 
 
      if (!is_empty_molecule(molecule)) {

	do {
	  boundPrivilege += molecule->P_value; 
	  ++instructions;
	  ++molecule;
	  if ( molecule >= qcw->molecules+def_2_to_W) {
	    fprintf(stderr, "INVALID META FILE\n"); 
	    Exit (QCWFERR); 
          }
	  
	}while (!is_empty_molecule(molecule));

	add_genotype (gen_begin, molecule); 
      }
    }
    
    //reset buf; discard $; get next line 
    *buf = '0'; 
    fgets (buf, MAXALLCHAR, filep);  
    fgets (buf, MAXALLCHAR, filep);
  }  

  genotype = qcw->genotypes; 

  popTotal = popRep = popCon = popQuant = popNone = 0; 

  instVerify = 0; 

  while (genotype) {
  	     
    popTotal += genotype->freq; 

    isRep = isRep1 = isRep2 = isCon = isQuant = isNone = 0; 

    instCount = 0; 

    for (i = 0; i < genotype->length; i++) { 

      if (genotype->backbone[i].opcode == 
	  (FIELD_T) MOV *256 + (FIELD_T) mId ) 
	isRep1 = 1; 

      if ((genotype->backbone[i].opcode & 0xFF) == (FIELD_T) mPd ||
          (genotype->backbone[i].opcode & 0xFF) == (FIELD_T) mPde  ) 
	isRep2 = 1; 
      
      if (genotype->backbone[i].opcode & 2) 
	isCon = 1; 

      if ((genotype->backbone[i].opcode >>8) == QOP || 
          (genotype->backbone[i].opcode >>8) == QCN) { 
	isQuant = 1; 
      }
    } 
    instVerify += genotype->length * genotype->freq; 

    if (isRep1 && isRep2) {
      popRep += genotype->freq; 
      isRep = 1;
    }

    if (isCon)
      popCon += genotype->freq; 

    if (isQuant) 
      popQuant += genotype->freq; 

    if (!isRep && !isCon &!isQuant) {
      popNone += genotype->freq; 
      isNone = 1;
    }
    fprintf (STDOUT, 
	     "G MD5 %016llx%016llx FREQ %lld R %d C %d Q %d N %d\n",
	     genotype->md5high, genotype->md5low, genotype->freq,
	     isRep, isCon, isQuant, isNone); 

    genotype = genotype->next;  
  }

  if (instructions != instVerify) {
    fprintf(stderr, "g. freq. * g. len != t. inst %lld != %lld\n",
	    instVerify, instructions); 
    Exit (INTERNALERR); 
  }

  fprintf (STDOUT,
   "G SUMMARY T %lld R %lld C %lld Q %lld N %lld I %lld B %lld F %lld\n", 
	   popTotal, popRep, popCon, popQuant, popNone,  
           instructions, boundPrivilege, freePrivilege ); 

  checkProc = 0;
  checkBound = 0; 

  for ( i = 0; i < 2048; i++ ) {
    fprintf (STDOUT, 
	     "G B%d %d %d\n", i, brick[i], bound[i]); 
    checkProc += brick[i]; 
    checkBound += bound[i];  
  }
  if (checkProc != instructions || checkBound != boundPrivilege) {
    fprintf(stderr, "checkProc ***%d*** checkBound *** %d *** META ERROR\n",
	    checkProc, checkBound); 
    Exit (QCWFERR); 
  }

}
 
void read_qcw_file (FILE *filep)
{
  char buf[MAXALLCHAR]; 
  mem_struct * molecule, * quad; 

  unsigned long long md5high0, md5low0, md5high1, md5low1; 
  int Q_value, i, quadSize, is_new; 

  // Climate 

  do {
    fgets(buf, MAXALLCHAR, filep);
  }while (strncmp (buf, "C ", 2));
  sscanf (buf, "C W %ld", &qcw->C_W);
 
  fgets(buf, MAXALLCHAR, filep);
  sscanf (buf, "C X %ld", &qcw->C_X);

  fgets(buf, MAXALLCHAR, filep);
  sscanf (buf, "C Y %ld", &qcw->C_Y);

  fgets(buf, MAXALLCHAR, filep);  
  sscanf (buf, "C Z %ld", &qcw->C_Z);
  
  fgets(buf, MAXALLCHAR, filep);
  sscanf (buf, "C M %ld", &qcw->C_M);
   
  fgets(buf, MAXALLCHAR, filep);
  sscanf (buf, "C F %lld", &qcw->C_F);
 
  write_world_climate (NULL, &md5high0, &md5low0); 

  fgets(buf, MAXALLCHAR, filep);
  if (sscanf(buf, ";MD5 %016llx%016llx", &md5high1, &md5low1) != 2
      || md5high0 != md5high1 || md5low0 != md5low1) {
    fprintf(stderr, "Error parsing (C)limate: %s", buf);
    Exit (QCWFERR); 
  } 
  fprintf (STDOUT, ";MD5 %016llx%016llx C(0)\n", md5high0, md5low0);

  // Compute derived constants

  qcw->C_C = qcw->C_W - qcw->C_X - qcw->C_Y - qcw->C_Z;


  // Randomness 

  fgets(buf, MAXALLCHAR, filep); 
  sscanf (buf, "R %8x%8x", 
	  (unsigned int *) &qcw->ctx->randcnt, 
	  (unsigned int *) &qcw->ctx->randa);

  fgets(buf, MAXALLCHAR, filep); 
  sscanf (buf, "R %8x%8x", 
	  (unsigned int *) &qcw->ctx->randb, 
	  (unsigned int *) &qcw->ctx->randc);

  for (i = 0; i < RANDSIZ; i++) {
    fgets(buf, MAXALLCHAR, filep); 
    sscanf (buf, "R %8x%8x",
	    (unsigned int *) &qcw->ctx->randrsl[i],
	    (unsigned int *) &qcw->ctx->randmem[i]);
  }
  
  write_world_randomness (NULL, &md5high0, &md5low0); 

  fgets(buf, MAXALLCHAR, filep);
  if (sscanf(buf, ";MD5 %016llx%016llx", &md5high1, &md5low1) != 2
      || md5high0 != md5high1 || md5low0 != md5low1) {
    fprintf(stderr, "Error parsing (R)andomness: %s", buf);
    Exit (QCWFERR); 
  } 
  fprintf (STDOUT, ";MD5 %016llx%016llx R(0)\n", md5high0, md5low0);


  // Molecules

  quadSize = POWER_OF_TWO (qcw->C_W-(qcw->C_X+qcw->C_Y+qcw->C_Z+2));

  for ( quad = qcw->molecules; 
	quad < qcw->molecules+def_2_to_W; 
	quad += quadSize) {
    
    //skip first molecule in quad 
    fgets (buf, MAXALLCHAR, filep);     
    set_empty_molecule (quad); 

    for ( molecule = quad+1; molecule < quad+quadSize-1; molecule++) { 
    
      fgets (buf, MAXALLCHAR, filep);
      
      if (!strncmp (buf, "M\n", 2)) {
	set_empty_molecule (molecule); 
	Q_value = 0; 
      }
      else if (sscanf(buf,"M %4hx%2hx%2hx%16llx%16llx %x %x %llx %d",
		      &molecule->opcode, 
		      &molecule->A_mode, 
		      &molecule->B_mode,
		      &md5high0,
		      &md5low0, 
		      &molecule->A_value, 
		      &molecule->B_value, 
		      &molecule->P_value,
		      &Q_value) == 9) { 
	molecule->tag = new_tag (md5high0, md5low0, &is_new);
      }
      else {
	fprintf(stderr, "Error parsing (M)olecules: %s", buf);
	Exit(QCWFERR); //must be empty or biotic 
      }
      
      if (Q_value == -1) {
	fprintf(stderr, "Error parsing Qubits in (M)olecules: %s", buf);
	Exit (QCWFERR); // can't read qubits from file (yet)
      }
      molecule->Q_value = Q_value;
      molecule->Q_alloc = 0;
      molecule->Q_index = 0; 
      molecule->Q_block = 0;    
      molecule->debuginfo = 0; 
      
    }
    //skip last molecule in quad 
    fgets (buf, MAXALLCHAR, filep);     
    set_empty_molecule (molecule);
  }
  write_world_molecules (NULL, &md5high0, &md5low0); 
  
  fgets(buf, MAXALLCHAR, filep);
  if (sscanf(buf, ";MD5 %016llx%016llx", &md5high1, &md5low1) != 2
      || md5high0 != md5high1 || md5low0 != md5low1) {
    fprintf(stderr, "Error checking MD5 in (M)olecules: %s", buf);
    Exit (QCWFERR); 
    } 
  fprintf (STDOUT, ";MD5 %016llx%016llx M(0)\n", md5high0, md5low0);
  
  qcw->init = 1;   
}

void molecule_to_cartesian (mem_struct *molecule, 
			    int *x, int *y, int *z, int *c) {
  int temp; 
  
  temp = molecule-qcw->molecules;   
  *c = (temp & POWER_OF_TWO(qcw->C_C-1));  
  
  temp >>= qcw->C_C; 
  *z = (temp & POWER_OF_TWO(qcw->C_Z-1)); 
  
  temp >>= qcw->C_Z; 
  *y = (temp & POWER_OF_TWO(qcw->C_Y-1)); 

  temp >>= qcw->C_Y; 
  *x = (temp & POWER_OF_TWO(qcw->C_X-1)); 
}

mem_struct * cartesian_to_molecule (int x, int y, int z, int c ) {
  
  return (qcw->molecules+((x<<(qcw->C_C+qcw->C_Z+qcw->C_Y)) | 
			  (y<<(qcw->C_C+qcw->C_Z))|(z<<(qcw->C_C)) | c)); 
}


int is_empty_molecule (mem_struct *molecule) {
  
  if ((molecule->opcode != (FIELD_T) DAT *256 + (FIELD_T) mF) ||
      (molecule->A_mode != (FIELD_T) DIRECT) ||
      (molecule->B_mode != (FIELD_T) DIRECT) ||
       molecule->A_value || 
       molecule->B_value || 
       molecule->debuginfo ||	   
       molecule->tag ||
       molecule->Q_alloc || 
       molecule->Q_value || 
       molecule->P_value) {
    return (0);   
  }
  else {
    return (1); 
  }
}


void set_empty_molecule (mem_struct *molecule) {
  
  molecule->opcode = (FIELD_T) DAT *256 + (FIELD_T) mF;
  molecule->A_mode = molecule->B_mode = (FIELD_T) DIRECT;
  molecule->A_value = molecule->B_value = molecule->P_value = 0;

  molecule->debuginfo = 0; 

  //no reference count so must do garbage collection (when?!?)  
  molecule->tag = NULL; 

  //do quantum more carefully 
  molecule->Q_alloc = molecule->Q_value = 0;
  molecule->Q_index = molecule->Q_block = 0; 
}

void set_empty_molecule_adj_priv (mem_struct *molecule) {
  
  if (molecule->tag) {
    --qcw->tP; 
    ++qcw->C_F; 
    molecule->tag = NULL;  // garbage collect?!?  
  }
  qcw->bP -= molecule->P_value;     
  
  qcw->C_F += molecule->P_value;
  molecule->P_value = 0; 

  molecule->opcode = (FIELD_T) DAT *256 + (FIELD_T) mF;
  molecule->A_mode = molecule->B_mode = (FIELD_T) DIRECT;
  molecule->A_value = molecule->B_value = 0;

  molecule->debuginfo = 0; 

  //do quantum more carefully 
  molecule->Q_alloc = molecule->Q_value = 0;
  molecule->Q_index = molecule->Q_block = 0; 
}


