#!/usr/bin/perl -w

use POSIX;
use constant PI => 3.14159265358979;

$wDim = 4;
$xDim = 8;
$yDim = 8; 
$zDim = 8; 

open (SLURP, 'grep "G B" */summary.txt | wc|');

while (<SLURP>) {
    chomp;
    @num = split;    
    $tPts = $num[0]/2048; 
}


$size = sprintf ("%.2f", ($xDim * $wDim) / (2 * PI));

print 
    "#VRML V2.0 utf8\n".
    "Viewpoint { position 0 0 15 }\n".
    "DEF TIMER TimeSensor {cycleInterval $tPts loop TRUE}\n".
    "Transform {\n".
    "translation 0 0 0 children [\n".
    "Shape {\n".
    "appearance Appearance {\n".
    "material DEF blah Material {\n". 
    "diffuseColor .3 .3 .3\n".  
    "}\n".
    "}".
    "geometry Sphere {radius $size }\n". 
    "}".
    "]".
    "}\n".
    "Transform {\n".
    "rotation 0 1 0 1.57\n".
    "translation 0 .1 0 children [\n". 
    "Shape {\n".
    "appearance Appearance {\n".
    "material Material {diffuseColor 1.0 1.0 1.0 }\n".
    "}\n".
    "geometry Cylinder {height 120.0 radius 0.1}\n".
    "}\n".
    "]\n".
    "}\n".
    "Transform {\n".
    "rotation 1 0 0 1.57\n".
    "translation 0 0 .1 children [\n". 
    "Shape {\n".
    "appearance Appearance {\n".
    "material Material {diffuseColor 1.0 1.0 1.0 }\n".
    "}\n".
    "geometry Cylinder {height 120.0 radius 0.1}\n".
    "}\n".
    "]\n".
    "}\n".
    "Transform {\n".
    "rotation 0 0 1 1.57\n".
    "translation .1 0 0 children [\n". 
    "Shape {\n".
    "appearance Appearance {\n".
    "material Material {diffuseColor 1.0 1.0 1.0 }\n".
    "}\n".
    "geometry Cylinder {height 120.0 radius 0.1}\n".
    "}\n".
    "]\n".
    "}\n";

$count = 0; 

for ($w = 0; $w < $wDim; $w++) {
    for ($x = 0; $x < $xDim; $x++) {
	for ($y = 0; $y < $yDim; $y++) {
	    for ($z = 0; $z < $zDim; $z++) {
		print "\n#$count\n\n";		 

		$xPos = $w * $xDim + $x; 
		$xMax = $wDim * $xDim; 

		$i = sprintf("%.2f", 
			     ($z+$size) * sin (2 * PI / $xMax * $xPos )); 
		$j = sprintf("%.2f", 
			     $y-$yDim/2+0.5);
		$k = sprintf("%.2f", 
			     ($z+$size) * cos (2 * PI / $xMax * $xPos )); 

		print "DEF cw".$w."x".$x."y".$y."z".$z.
		    " ColorInterpolator {\nkey [";   	
		for ($t = 0; $t < $tPts+2; $t++) { 
		    $item = sprintf "%.2f", $t/($tPts+1);
		    print "$item "; 
		}	
		print "]\nkeyValue ["; 
		 
		open (SLURP, 'grep "G B'.$count.' " */summary.txt | sort -n|');

		$check = $tPts; 
		while (<SLURP>) {
		    chomp;
		    @num = split;    
		    $R = $num[2]/32; 
		    $G = $num[3]/32; 
		    $B = 0;
		    if ($G > 1) {
			$G = 1;
		    }
		    print "\n $R $G $B,";
		    $check--;
		}
		if ($check) {
		    die "$count $tPts $check\n"; 
		}
		
		print "\n 0 0 0,\n 0 0 0]\n}\nTransform {\n".
		    " translation ".$i." ".$j." ".$k. 
		    " children [\n".
		    "  Shape {\n".
		    "   appearance Appearance {\n".
		    "    material DEF bw".$w."x".$x."y".$y."z".$z." Material {\n".
		    "     diffuseColor 0 0 0\n".
		    "     transparency 0.5\n".
		    "    }\n".
		    "   }\n".
		    "   geometry Box {size 1 1 1 }\n".
		    "  }\n".
		    " ]\n".
		    "}\n".
		    "ROUTE TIMER.fraction_changed TO ". 
		    "cw".$w."x".$x."y".$y."z".$z.".set_fraction\n".
		    "ROUTE cw".$w."x".$x."y".$y."z".$z.".value_changed TO ".
		    "bw".$w."x".$x."y".$y."z".$z.".set_diffuseColor\n\n";  
		
		$count++; 

	    }
	}
    }
}
    
