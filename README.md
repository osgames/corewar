# Core War

Originally developed at https://sourceforge.net/projects/corewar/ by [alexanderwait](http://sourceforge.net/users/alexanderwait), [amarsden](http://sourceforge.net/users/amarsden) and [bjoerngruenzel](http://sourceforge.net/users/bjoernguenzel) and published under the GPL-2.0/2-clause BSD license.